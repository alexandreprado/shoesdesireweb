<?php

class configs {

    private $pag;
    //FUNCOES PARA AS ESTATISTICAS
    public $gaID = "41369720"; //ID DO GOOGLE ANALITCS
    //DATA DE INICIO NO GOOGLE ANALITCS
    public $data_inicio = "2011-01-21";

    function getID() {
        return ($this->gaID);
    }

    function getAno() {
        return $this->data_inicio;
    }

    //CONFIGURACOES DE LISTAGEM
    private $edit = "<img src='gfx/icones/editar.png' width='16' height='16'/>";
    private $del = "[X]";
    private $novo = "<img src='gfx/botao.gif' width='66' height='15' style:'border:none;' />";
    //MENSAGEM DE SUCESSO
    private $mensagensSGC = "
		<!-- MENSAGEM DE SUCESSO -->
		<div id='mensagem_sucesso' style='display:none; position: fixed'>
			<span>Sua a&ccedil;&atilde;o foi realizada com sucesso!</span>
		</div>
	
		<!-- MENSAGEM DE ERRO -->
		<div id='mensagem_erro' style='display:none; position: fixed'>
			<span>Houve um erro ao tentar realizar a a&ccedil;&atilde;o! Tente novamente.</span>
		</div>
	";
    private $mensagensLogin = "
	
		<!-- MENSAGEM DE ERRO -->
		<div id='login_erro' style='display:none;'>
			<span>Usuário e/ou senha inválidos.</span>
		</div>
                
                <!-- MENSAGEM DE ERRO -->
		<div id='nao_logado' style='display:none;'>
			<span>É preciso estar logado para acessar este conteúdo.</span>
		</div>
	";
    
    private $mensagemErro = "
	
		<!-- MENSAGEM DE ERRO -->
		<div id='erro_operacao' style='display:none;'>
			<span>Ocorreu um erro ao executar a operação</span>
		</div>
	";
    
    private $mensagemSucesso = "
	
		<!-- MENSAGEM DE SUCESSO  -->
		<div id='sucesso_operacao' style='display:none;'>
			<span>Operação realizada com sucesso!</span>
		</div>
	";
    
    private $mensagemNavegador = "
	
		<!-- MENSAGEM DE ERRO -->
		<div id='navegador_erro' style='display:none;'>
			<span>A versão do seu navegador não suporta a exibição deste website</span>
		</div>
	";
    
    private $mensagemDel = "<!-- MENSAGEM DE DELETE -->
		<div id='confirma' style='display:none;'>
			<img src='gfx/warning.png' width='32' height='28' alt='Ok'/> Registro(s) excluido(s) com sucesso!
		</div>";
    private $scriptLoginErro = "<script>
		$(document).ready(function () {
			$('#login_erro').fadeIn(1000, function() {
					setTimeout( function()  {
						$('#login_erro').fadeOut(600); 
					}, 4000); 
			});  
		}); 
		</script>";
    private $scriptNaoLogado = "<script>
		$(document).ready(function () {
			$('#nao_logado').fadeIn(1000, function() {
					setTimeout( function()  {
						$('#nao_logado').fadeOut(600); 
					}, 4000); 
			});  
		}); 
		</script>";
    private $scriptNavegador = "<!-- MENSAGEM DE ERRO DE NAVEGADOR -->
        <script>
		$(document).ready(function () {
			$('#navegador_erro').fadeIn(1000); 
                        });
		</script>";
    private $scriptSucesso = "<script>
		$(document).ready(function () {

			$('#mensagem_sucesso').fadeIn(1000, function() {
				setTimeout( function()  {
					$('#mensagem_sucesso').fadeOut(600); 
				}, 4000); 
			});
		
		});  
		</script>";
    private $scriptErro = "<script>
		$(document).ready(function () {
			$('#mensagem_erro').fadeIn(1000, function() {
					setTimeout( function()  {
						$('#mensagem_erro').fadeOut(600); 
					}, 4000); 
			});  
		}); 
		</script>";
    private $scriptDuplicacao = "<script>
		$(document).ready(function () {
			$('.duplicacao').fadeIn( function() {
					setTimeout( function()  {
						$('.duplicacao').fadeOut('fast'); 
					}, 2000); 
			});  
		}); 
		</script>";
    private $scriptDel = "<script>
		$(document).ready(function () {
			$('#confirma').fadeIn( function() {
					setTimeout( function()  {
						$('#confirma').fadeOut('fast'); 
					}, 2000); 
			});  
		}); 
		</script>";
    private $nomeCliente = "";
    private $siteCliente = "";
    private $breadcumb = "<a href='javascript:void(0)' onclick=\"loadPage('home')\">Sistema Gerenciador de Conte&uacute;do</a> &raquo;";

    //NOME DO CLIENTE
    function getCliente() {
        echo $this->nomeCliente;
    }

    //SITE CLIENTE
    function getSite() {
        echo $this->siteCliente;
    }

    //BREADCUMB
    function getBread() {
        echo $this->breadcumb;
    }

    //MENSAGENS DAS P�GINAS
    function getMensagem() {
        echo $this->mensagensSGC;
    }

    function getMensagemLogin() {
        echo $this->mensagensLogin;
    }
    
    function getMensagemNavegador() {
        echo $this->mensagemNavegador;
    }
    
    function getMensagemErro() {
        echo $this->mensagemErro;
    }
    
    function getError($mensagem){
        echo "<script>
		$(document).ready(function () {
                $('#erro_operacao span').html('$mensagem');
			$('#erro_operacao').fadeIn(500, function() {
					setTimeout( function()  {
						$('#erro_operacao').fadeOut(500); 
					}, 2000); 
			});  
		}); 
		</script>";
    }
    
    function getSucess(){
        echo "<script>
		$(document).ready(function () {
			$('#sucesso_operacao').fadeIn(500, function() {
                            setTimeout( function()  {
                                    $('#sucesso_operacao').fadeOut(500); 
                            }, 2000); 
			});  
		}); 
		</script>";
    }
    
    function getMensagemSucesso() {
        echo $this->mensagemSucesso;
    }
    
    function getErroNavegador() {
        echo $this->scriptNavegador;
    }

    function getErroLogin() {
        echo $this->scriptLoginErro;
    }

    function getNaoLogado() {
        echo $this->scriptNaoLogado;
    }

    function getMensagemDel() {
        echo $this->mensagemDel;
    }

    function getSucesso() {
        echo $this->scriptSucesso;
    }

    function notify($title, $message) {
        echo "<script>
		$(document).ready(function () {

			$.ambiance({message: '$message',
                        title: '$title',
                        timeout: 5,
                        type: 'success'});
		
		});  
		</script>";
    }

    function notifyError($title, $message) {
        echo "<script>
		$(document).ready(function () {

			$.ambiance({message: '$message',
                        title: '$title',
                        timeout: 5,
                        type: 'error'});
		
		});  
		</script>";
    }
    
    function notifySuccessPermanent($title, $message) {
        echo "<script>
		$(document).ready(function () {

			$.ambiance({message: '$message',
                        title: '$title',
                        timeout: 0,
                        type: 'success'});
		
		});  
		</script>";
    }
    
    function notifyErrorPermanent($title, $message) {
        echo "<script>
		$(document).ready(function () {

			$.ambiance({message: '$message',
                        title: '$title',
                        timeout: 0,
                        type: 'error'});
		
		});  
		</script>";
    }

    function getLoginErro() {
        
    }

    function getErro() {

//		if($existe != ""){
//			echo $this->scriptDuplicacao;	
//		}else{
        echo $this->scriptErro;
//		}
    }

    function getDelete() {
        echo $this->scriptDel;
    }

    //ENVIA EMAIL COM A SENHA
    function enviaMail($nome, $email, $senha) {

        $para = $email;

        $headers = "Content-Type: text/html; charset=utf-8\n";

        $headers.="From: \n";

        $corpo = "<span style='font-family:Arial;font-size:12px;'> Ol� " . $nome . ", voc� foi cadastrado no SGC com sucesso! Sua senha de acesso �: <strong>" . $senha . "</strong>   <br><br>Aten��o: guarde essa senha em um lugar seguro e n�o passe para ninguem.</span>";


        mail("$para", "Acesso SGC.", "$corpo", "$headers");
    }

    //IMAGENS DA LISTAGEM
    function getEdit($permissao) {

        if ($permissao > 2) {
            echo $this->edit;
        }
    }

    function getDel($permissao) {

        if ($permissao > 1) {
            echo $this->del;
        }
    }

    function getNew($permissao) {

        if ($permissao > 0) {
            echo $this->novo;
        }
    }

    //REGISTRA LOG NO SISTEMA
    function setLog($usuario, $acao) {

        $data = date("Y-m-d");
        $hora = date("H:i:s");

        $sql = mysql_query("INSERT INTO sgc_relatorios (usuario, acao, data, hora) VALUES ('" . $usuario . "','" . $acao . "','" . $data . "','" . $hora . "')");
    }

}
?> 
