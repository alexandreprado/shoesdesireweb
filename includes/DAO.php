<?php

class DAO {

    static private $instance;
    static private $pdo;
    private $tabela;
    private $atributos;
    private $atributo1;
    private $atributo2;
    private $valores;
    private $condicao;
    private $condicao1;
    private $condicao2;
    private $atributo;

    /**
     * @return PDO
     */
    private function getPDO() {
        return self::$pdo;
    }

    private function setPDO($pd) {
        self::$pdo = $pd;
    }

    private function __construct() {
        try {
            $pdo = new PDO("mysql:host=localhost;dbname=bd_justdoctor", "justdoctor_user", "justdoctor123");
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->setPDO($pdo);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    /**
     * @return DAO
     */
    static public function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new DAO();
        }
        return self::$instance;
    }

    public function getRows($tabela) {
        try {
            $stmte = $this->getPDO()->prepare("SELECT * FROM $tabela");
            $executa = $stmte->execute();

            $array = array();

            if ($executa) {
                while ($reg = $stmte->fetch(PDO::FETCH_ASSOC)) { /* Para recuperar um ARRAY utilize PDO::FETCH_ASSOC */
                    $array[] = $reg;
                }
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        return $array;
    }

    public function getRowsByCondition($tabela, $atributo, $condicao) {
        try {
            $stmte = $this->getPDO()->prepare("SELECT * FROM $tabela WHERE $atributo = $condicao");
            $executa = $stmte->execute();

            $array = array();

            if ($executa) {
                while ($reg = $stmte->fetch(PDO::FETCH_ASSOC)) { /* Para recuperar um ARRAY utilize PDO::FETCH_ASSOC */
                    $array[] = $reg;
                }
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        return $array;
    }

    public function getRowsOrderBy($tabela, $orderBy) {
        try {
            $stmte = $this->getPDO()->prepare("SELECT * FROM $tabela ORDER BY $orderBy");
            $executa = $stmte->execute();

            $array = array();

            if ($executa) {
                while ($reg = $stmte->fetch(PDO::FETCH_ASSOC)) { /* Para recuperar um ARRAY utilize PDO::FETCH_ASSOC */
                    $array[] = $reg;
                }
            } else {
                echo 'Nenhum registro encontrado';
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        return $array;
    }

    function Inserir($usuario_id, $tabela, $atributos, $valores) {

        try {

            $this->tabela = $tabela;
            $this->atributos = $atributos;
            $this->valores = $valores;
            $tamanho = count($this->atributos);
            $str = "insert into $tabela (";
            for ($i = 0; $i < $tamanho; $i++) {
                $str .= $this->atributos[$i];
                if ($i < $tamanho - 1) {
                    $str .= ",";
                }
            }

            $str = $str . ") values (";
            for ($j = 0; $j < $tamanho; $j++) {
                $str .= "?";
                if ($j < $tamanho - 1) {
                    $str .= ",";
                }
            }
            $str = $str . ")";

            $stmte = $this->getPDO()->prepare($str);

            for ($k = 1; $k <= $tamanho; $k++) {
                $stmte->bindParam($k, $this->valores[$k - 1], PDO::PARAM_STR);
            }

            $stmte->execute();

            $id = $this->getPDO()->lastInsertId();

            $this->gravaLog($usuario_id, Utils::$LOG_INFO, "Inseriu registro com id $id na tabela $tabela");
        } catch (PDOException $e) {
            $this->gravaLog($usuario_id, Utils::$LOG_ERROR, "Erro ao inserir registro na tabela $tabela: " . $e->getMessage());
            throw new Exception($e->getMessage());
        }

        return $id;
    }

    function Excluir($usuario_id, $tabela, $atributos, $condicao) {

        try {
            $this->tabela = $tabela;
            $this->condicao = $condicao;
            $this->atributos = $atributos;

            $stmte = $this->getPDO()->prepare("DELETE FROM $this->tabela WHERE $this->atributos = $this->condicao");
            $stmte->execute();

            $this->gravaLog($usuario_id, Utils::$LOG_INFO, "Excluiu registro da tabela $tabela onde $condicao = $atributos");
        } catch (Exception $e) {
            $this->gravaLog($usuario_id, Utils::$LOG_ERROR, "Erro ao excluir registro da tabela $tabela onde $atributos = $condicao: " . $e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    function Excluir2Argumentos($usuario_id, $tabela, $atributo1, $atributo2, $condicao1, $condicao2) {

        try {
            $this->tabela = $tabela;
            $this->condicao1 = $condicao1;
            $this->condicao2 = $condicao2;
            $this->atributo1 = $atributo1;
            $this->atributo2 = $atributo2;

            $stmte = $this->getPDO()->prepare("DELETE FROM $this->tabela WHERE $this->atributo1 = $this->condicao1 AND $this->atributo2 = $this->condicao2");
            $stmte->execute();

            $this->gravaLog($usuario_id, Utils::$LOG_INFO, "Excluiu registro da tabela $tabela onde $condicao1 = $atributo1 e $condicao2 = $atributo2");
        } catch (Exception $e) {
            $this->gravaLog($usuario_id, Utils::$LOG_ERROR, "Erro ao excluir registro da tabela $tabela onde $condicao1 = $atributo1 e $condicao2 = $atributo2: " . $e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    function Alterar($usuario_id, $tabela, $atributos, $valores, $condicao, $atributo) {

        try {
            $this->tabela = $tabela;
            $this->atributos = $atributos;
            $this->valores = $valores;
            $this->condicao = $condicao;
            $this->atributo = $atributo;
            $tamanho = count($this->atributos);
            $str = "UPDATE $tabela set ";
            for ($i = 0; $i < $tamanho; $i++) {
                $str .= $this->atributos[$i] . " = ?";
                if ($i < $tamanho - 1) {
                    $str .= ", ";
                }
            }
            $str .= " WHERE $this->condicao = $this->atributo";

            $stmte = $this->getPDO()->prepare($str);

            for ($k = 1; $k <= $tamanho; $k++) {
                $stmte->bindParam($k, $this->valores[$k - 1], PDO::PARAM_STR);
            }

            $stmte->execute();

            $this->gravaLog($usuario_id, Utils::$LOG_INFO, "Alterou registro da tabela $tabela onde $condicao = $atributo");
        } catch (Exception $e) {
            $this->gravaLog($usuario_id, Utils::$LOG_ERROR, "Erro ao alterar registro da tabela $tabela onde $condicao = $atributo: " . $e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    public function login($user, $senha) {

        try {
            $stmte = $this->getPDO()->prepare("SELECT * FROM usuario WHERE usuario = ? and senha = ?");
            $stmte->bindParam(1, $user, PDO::PARAM_STR);
            $stmte->bindParam(2, $senha, PDO::PARAM_STR);
            $executa = $stmte->execute();

            $array = array();

            if ($executa) {
                while ($reg = $stmte->fetch(PDO::FETCH_ASSOC)) { /* Para recuperar um ARRAY utilize PDO::FETCH_ASSOC */
                    $array[] = $reg;
                }
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
        return $array;
    }

    public function startSession($usuarioID, $token, $data_session) {

        try {
            $stmte = $this->getPDO()->prepare("UPDATE usuario SET token = ?, session_stamp = ? WHERE id = ?");
            $stmte->bindParam(1, $token, PDO::PARAM_STR);
            $stmte->bindParam(2, $data_session, PDO::PARAM_STR);
            $stmte->bindParam(3, $usuarioID, PDO::PARAM_STR);
            $stmte->execute();

            if ($stmte->rowCount() > 0) {
                return true;
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }

    public function log($message) {

        try {
            $stmte = $this->getPDO()->prepare("INSERT INTO log (text, time_created) VALUES (?, '" . date('Y-m-d H:i') . "')");
            $stmte->bindParam(1, $message, PDO::PARAM_STR);
            $stmte->execute();

            if ($stmte->rowCount() > 0) {
                return true;
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }

    function validaUsuario($username, $senha) {
        try {

            $str = "SELECT * FROM usuario WHERE username = ? AND senha = ?";

            $stmte = $this->getPDO()->prepare($str);
            $stmte->bindParam(1, $username, PDO::PARAM_STR);
            $stmte->bindParam(2, $senha, PDO::PARAM_STR);

            $result = $stmte->execute();

            $array = array();

            if ($result) {
                while ($reg = $stmte->fetch(PDO::FETCH_ASSOC)) { /* Para recuperar um ARRAY utilize PDO::FETCH_ASSOC */
                    $array[] = $reg;
                }
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        return $array;
    }

    function gravaLog($usuario_id, $tipo, $message) {

        try {

            $dt_log = date('Y-m-d H:i:s');

            $stmte = $this->getPDO()->prepare("INSERT INTO log (usuario_id, tipo_log, dt_log, message) VALUES (?, ?, ?, ?)");

            $stmte->bindParam(1, $usuario_id, PDO::PARAM_INT);
            $stmte->bindParam(2, $tipo, PDO::PARAM_STR);
            $stmte->bindParam(3, $dt_log, PDO::PARAM_STR);
            $stmte->bindParam(4, $message, PDO::PARAM_STR);

            $stmte->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function salvaFotoPaciente($paciente_id, $imagem, $path, $complete_path) {

        try {

            $origem = $complete_path . '/uploads/photos/' . $imagem;

            $pic = explode('.', $imagem);

            $image_name_body = "patient_" . $paciente_id;
            $image_name_ext = $pic[1];

            $new_image_name = $image_name_body . "." . $image_name_ext;

            $dirArquivos = $complete_path . '/uploads/photos/' . $paciente_id;

            if (!is_dir($dirArquivos)) {
                mkdir($dirArquivos, 0777);
            }

            foreach (glob($dirArquivos . '/*') as $file) {
                unlink($file);
            }

            if ($imagem) {

                $path = $path . '/uploads/photos/' . $paciente_id . "/" . $new_image_name;

                $destino = $dirArquivos . "/" . $new_image_name;
                copy($origem, $destino);
                unlink($origem);

                $this->Alterar($_SESSION['login']['id'], 'paciente', array("foto"), array($path), "id", $paciente_id);
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }

}

?>
