<?php

include("class.upload.php");

$uploaddir = '../uploads/user_picture/';

$foo = new Upload($_FILES['uploadfile']);

$foo->file_new_name_body = 'photo_temp_' . time();
$foo->image_convert = 'png';
$foo->image_resize = true;
$foo->image_x = 400;
$foo->image_y = 400;
$foo->image_ratio = true;
$foo->file_overwrite = true;
$foo->file_auto_rename = false;

$foo->Process($uploaddir);

if ($foo->processed) {
    echo json_encode(array("result" => true, "image_x" => $foo->image_dst_x, "image_y" => $foo->image_dst_y, "image" => $foo->file_dst_name));
} else {
    echo json_encode(array("result" => false, "error" => $foo->error));
}
?>