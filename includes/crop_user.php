<?php

require_once('class.upload.php');

$handle = new Upload($_SERVER['DOCUMENT_ROOT'] . $_POST['tempfile']);

//if ($handle->uploaded) {
$handle->file_src_name_body = "cropped_" . time(); // hard name
$handle->file_overwrite = true;
$handle->file_auto_rename = false;
$handle->image_resize = true;
$handle->image_x = 92; //size of final picture
$handle->image_y = 92; //size of final picture

$handle->jcrop = true;
$handle->rect_w = $_POST['image_w'];
$handle->rect_h = $_POST['image_h'];
$handle->posX = $_POST['image_x'];
$handle->posY = $_POST['image_y'];
$handle->jpeg_quality = 100;
$handle->Process($_SERVER['DOCUMENT_ROOT'] . '/uploads/user_picture/');

//thumb-50
//    $handle->file_src_name_body = $pic; // hard name
//    $handle->file_overwrite = true;
//    $handle->file_auto_rename = false;
//    $handle->image_resize = true;
//    $handle->image_x = 50;
//    $handle->image_y = 50; //size of picture
//
//    $handle->jcrop = true;
//    $handle->rect_w = $_POST['w'];
//    $handle->rect_h = $_POST['h'];
//    $handle->posX = $_POST['x'];
//    $handle->posY = $_POST['y'];
//    $handle->jpeg_quality = 100;
//    $handle->Process($_SERVER['DOCUMENT_ROOT'] . '/crop1.1/photos/50/');

$handle->clean();
unlink($_SERVER['DOCUMENT_ROOT'] . $_POST['tempfile']);

if ($handle->processed) {
    echo json_encode(array("result" => true, "image" => $handle->file_dst_name));
} else {
    echo json_encode(array("result" => false, "error" => $handle->error));
}
?>