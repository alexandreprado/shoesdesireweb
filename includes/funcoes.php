<?php

function get_header() {
    $INC_DIR = $_SERVER["DOCUMENT_ROOT"] . get_path() . "/templates";
    include "$INC_DIR/header.php";
}

function get_header_login() {
    $INC_DIR = $_SERVER["DOCUMENT_ROOT"] . get_path() . "/templates";
    include "$INC_DIR/header_login.php";
}

function get_footer() {
    $INC_DIR = $_SERVER["DOCUMENT_ROOT"] . get_path() . "/templates";
    include "$INC_DIR/footer.php";
}

function get_path() {
    return "";
}

function get_complete_path() {
    return $_SERVER["DOCUMENT_ROOT"] . get_path();
}

function remove_array_empty_values($array, $remove_null_number = true) {
    $new_array = array();

    $null_exceptions = array();

    foreach ($array as $key => $value) {
        $value = trim($value);

        if ($remove_null_number) {
            $null_exceptions[] = '0';
        }

        if (!in_array($value, $null_exceptions) && $value != "") {
            $new_array[] = $value;
        }
    }
    return $new_array;
}

function noInject($string) {
    $string = str_replace("or", "", $string);
    $string = str_replace("der", "", $string);
    $string = str_replace("by", "", $string);
    $string = str_replace("select", "", $string);
    $string = str_replace("delete", "", $string);
    $string = str_replace("create", "", $string);
    $string = str_replace("@", "", $string);
    $string = str_replace("union all", "", $string);
    $string = str_replace("drop", "", $string);
    $string = str_replace("update", "", $string);
    $string = str_replace("drop table", "", $string);
    $string = str_replace("show table", "", $string);
    $string = str_replace("applet", "", $string);
    $string = str_replace("object", "", $string);
    $string = str_replace("'", "", $string);
    $string = str_replace("#", "", $string);
    $string = str_replace("=", "", $string);
    $string = str_replace("--", "", $string);
    $string = str_replace("-", "", $string);
    $string = str_replace(";", "", $string);
    $string = str_replace("*", "", $string);
    $string = strip_tags($string);
    return $string;
}

// PRINT R formatado /////////////
function pr($string) {

    $formato = "<pre>" . print_r($string) . "</pre>";
    return($formato);
}

function get_url() {
    $ual = "http://noivafashion.com.br";
    return $ual;
}

function formatar($string) {

    $proibido = array(" ", "%", "&", "*");

    $str = str_replace($proibido, "-", $string);

    $formato = $str . ".html";

    return $formato;
}

function format_size($size) {
    $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
    if ($size == 0) {
        return('n/a');
    } else {
        return (round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]);
    }
}

/**
 * USAGE: mysql_safe( string $query [, array $params ] )
 * $query - SQL query WITHOUT any user-entered parameters. Replace parameters with "?"
 *     e.g. $query = "SELECT date from history WHERE login = ?"
 * $params - array of parameters
 *
 * Example:
 *    mysql_safe( "SELECT secret FROM db WHERE login = ?", array($login) );    # one parameter
 *    mysql_safe( "SELECT secret FROM db WHERE login = ? AND password = ?", array($login, $password) );    # multiple parameters
 * That will result safe query to MySQL with escaped $login and $password.
 * */
function mysql_safe($query, $params = false) {
    if ($params) {
        foreach ($params as &$v) {
            $v = mysql_real_escape_string($v);
        }    # Escaping parameters
        # str_replace - replacing ? -> %s. %s is ugly in raw sql query
        # vsprintf - replacing all %s to parameters
        $sql_query = vsprintf(str_replace("?", "'%s'", $query), $params);
        $sql_query = mysql_query($sql_query);    # Perfoming escaped query
    } else {
        $sql_query = mysql_query($query);    # If no params...
    }

    return ($sql_query);
}

function data($data) {
    $a = substr($data, 0, 4);
    $m = substr($data, 5, 2);
    $mes = "";

    if ($m == 1) {
        $mes = "Janeiro";
    } else if ($m == 2) {
        $mes = "Fevereiro";
    } else if ($m == 3) {
        $mes = "Mar&ccedil;o";
    } else if ($m == 4) {
        $mes = "Abril";
    } else if ($m == 5) {
        $mes = "Maio";
    } else if ($m == 6) {
        $mes = "Junho";
    } else if ($m == 7) {
        $mes = "Julho";
    } else if ($m == 8) {
        $mes = "Agosto";
    } else if ($m == 9) {
        $mes = "Setembro";
    } else if ($m == 10) {
        $mes = "Outubro";
    } else if ($m == 11) {
        $mes = "Novembro";
    } else if ($m == 12) {
        $mes = "Dezembro";
    }
    $d = substr($data, 8, 2);
    return $d . " de " . $mes . " de " . $a;
}

function getMes($data) {

    $m = substr($data, 5, 2);
    $mes = "";

    if ($m == 1) {
        $mes = "Janeiro";
    } else if ($m == 2) {
        $mes = "Fevereiro";
    } else if ($m == 3) {
        $mes = "Mar&ccedil;o";
    } else if ($m == 4) {
        $mes = "Abril";
    } else if ($m == 5) {
        $mes = "Maio";
    } else if ($m == 6) {
        $mes = "Junho";
    } else if ($m == 7) {
        $mes = "Julho";
    } else if ($m == 8) {
        $mes = "Agosto";
    } else if ($m == 9) {
        $mes = "Setembro";
    } else if ($m == 10) {
        $mes = "Outubro";
    } else if ($m == 11) {
        $mes = "Novembro";
    } else if ($m == 12) {
        $mes = "Dezembro";
    }
    return $mes;
}

function dataCompleta($data) {
    $a = substr($data, 0, 4);
    $m = substr($data, 5, 2);
    $d = substr($data, 8, 2);
    return $d . "." . $m . "." . $a;
}

///// NOME DA PAGINA ATUAL //
function pageConfig($pagina, $acao) {

    if ($acao == "inserir") {
        $pag = str_replace("_inserir.php", "", $pagina);
    }
    if ($acao == "fotos") {
        $pag = str_replace("_fotos.php", "", $pagina);
    }
    if ($acao == "inserirFoto") {
        $pag = str_replace("_fotos_inserir.php", "", $pagina);
    }
    if ($acao == "editarFoto") {
        $pag = str_replace("_fotos_editar.php", "", $pagina);
    }
    if ($acao == "editar") {
        $pag = str_replace("_editar.php", "", $pagina);
    }
    if ($acao == "detalhar") {
        $pag = str_replace("_detalhes.php", "", $pagina);
    }
    if ($acao == "empresa") {
        $pag = str_replace("_empresas.php", "", $pagina);
    }
    if ($acao == "") {
        $pag = str_replace(".php", "", $pagina);
    }

    return $pag;
}

function dia($data) {

    $d = substr($data, 8, 2);
    return $d;
}

function mes($data) {
    $m = substr($data, 5, 2);
    $mes = "";

    if ($m == 1) {
        $mes = "Jan";
    } else if ($m == 2) {
        $mes = "Fev";
    } else if ($m == 3) {
        $mes = "Mar";
    } else if ($m == 4) {
        $mes = "Abr";
    } else if ($m == 5) {
        $mes = "Maio";
    } else if ($m == 6) {
        $mes = "Jun ";
    } else if ($m == 7) {
        $mes = "Jul ";
    } else if ($m == 8) {
        $mes = "Ago";
    } else if ($m == 9) {
        $mes = "Set";
    } else if ($m == 10) {
        $mes = "Out";
    } else if ($m == 11) {
        $mes = "Nov";
    } else if ($m == 12) {
        $mes = "Dez";
    }
    return $mes;
}

function limita($string, $num) {

    $texto = strip_tags($string);
    $cont = strlen($texto);

    if ($cont > $num) {
        $txt = substr($texto, 0, $num) . "...";
        return $txt;
    } else {
        return $texto;
    }
}

function datamostra($data) {
    $a = substr($data, 0, 4);
    $m = substr($data, 5, 2);
    $d = substr($data, 8, 2);
    return $d . "/" . $m . "/" . $a;
}

function vdata($data) {
    $a = substr($data, 6, 4);
    $m = substr($data, 3, 2);
    $d = substr($data, 0, 2);
    return $a . "/" . $m . "/" . $d;
}

//error_reporting(E_NONE); 
//ini_set('display_errors', false); 


function getYTid($ytURL) {
    $ytvIDlen = 11; // This is the length of YouTube's video IDs
    // The ID string starts after "v=", which is usually right after
    // "youtube.com/watch?" in the URL
    $idStarts = strpos($ytURL, "?v=");

    // In case the "v=" is NOT right after the "?" (not likely, but I like to keep my
    // bases covered), it will be after an "&":

    if ($idStarts === FALSE)
        $idStarts = strpos($ytURL, "&v=");

    // If still FALSE, URL doesn't have a vid ID

    if ($idStarts === FALSE)
        die("YouTube video ID not found. Please double-check your URL.");

    // Offset the start location to match the beginning of the ID string

    $idStarts +=3;

    // Get the ID string and return it

    $ytvID = substr($ytURL, $idStarts, $ytvIDlen);
    return $ytvID;
}

function converterData($data) {

    $ar = explode(" ", $data);

    if (sizeof($ar) > 1) {

        $array = explode("-", $ar[0]);

        $novaData = $array[2] . "/" . $array[1] . "/" . $array[0] . " " . $ar[1];
    } else {
        $array = explode("-", $data);
        $novaData = $array[2] . "/" . $array[1] . "/" . $array[0];
    }

    return $novaData;
}

function desconverterData($data) {

    $ar = explode(" ", $data);

    if (sizeof($ar) > 1) {

        $array = explode("/", $ar[0]);

        $novaData = $array[2] . "-" . $array[1] . "-" . $array[0] . " " . $ar[1];
    } else {
        $array = explode("/", $data);
        $novaData = $array[2] . "-" . $array[1] . "-" . $array[0];
    }

    return $novaData;
}

function retira_acentos($texto) {
    $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
        , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç");
    $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
        , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C");
    return str_replace($array1, $array2, $texto);
}

function getLogin() {
    $INC_DIR = $_SERVER["DOCUMENT_ROOT"] . get_path() . "/includes";
    include "$INC_DIR/restritos.php";
}

function get_navegador() {
//Desenvolvido por Junior Zancan

    $ip = $_SERVER['REMOTE_ADDR']; //Pega o IP
    $navegador = $_SERVER['HTTP_USER_AGENT']; //Pega o navegador
//navegadores disponiveis:
    $ie6 = '/(MSIE 6.0)/i';
    $ie7 = '/(MSIE 7.0)/i';
    $ie8 = '/(MSIE 8.0)/i';
    $ff = '/(Firefox)/i';
    $op = '/(Opera)/i';
    $saf = '/(Safari)/i';
    $chrome = '/(Chrome)/i';

    if (preg_match($ie8, $navegador)) { //Verifica qual é o navegador
        $browser = "Internet Explorer 8";
    } elseif (preg_match($ie7, $navegador)) {
        $browser = "Internet Explorer 7";
    } elseif (preg_match($ie6, $navegador)) {
        $browser = "Internet Explorer 6";
    } elseif (preg_match($ff, $navegador)) {
        $browser = "Mozilla Firefox";
    } elseif (preg_match($op, $navegador)) {
        $browser = "Opera";
    } elseif (preg_match($saf, $navegador)) {
        $browser = "Safari";
    } elseif (preg_match($chrome, $navegador)) {
        $browser = "Google Chrome";
    } else {
        $browser = "Desconhecido";
    }

    return $browser;
}

function calcularIdade($data_nasc) {

    $data_nasc = explode("-", $data_nasc);
//
//    $data = date("Y-m-d");
//    $data = explode("-", $data);
    $anos = date('Y') - $data_nasc[0];

    if ($data_nasc[1] >= date('m')) {

        if ($data_nasc[2] <= date('d')) {
            return $anos;
        } else {
            return $anos - 1;
        }
    } else {
        return $anos;
    }
}
?> 
