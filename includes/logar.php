<?php

include 'DAO.php';

if (!isset($_SESSION)) {
    session_start();
}

$dao = DAO::getInstance();

// pegando dados do formulario
$username = str_replace("'", "", $_POST["username"]);
$senha = $_POST["password"];
$senha = md5($senha);

try {
    $usuario = $dao->validaUsuario($username, $senha);
} catch (Exception $e) {
    echo "Erro ao validar usuário: $e";
}

// verificando se encontrou registros do login e senha no banco de dados.
if ($usuario) {
    $id_usuario = $usuario[0]["id"];
    $user = $usuario[0]["username"];
    $nome = $usuario[0]["nome"];
    $perm = "3";
    $secret = md5("ThereIsASecretInsideAndNobodyKnowsThat"); // chave secreta

    $ip = $_SERVER["REMOTE_ADDR"]; // ip do usuario
    $hora = time(); // pegado horario atual.
    $chave = md5($id_usuario . $user . $nome . $perm . $secret . $ip . $hora);
    // registrando a session com um array com o codLogin, login e a chave.
    $_SESSION['login'] = array("id" => $id_usuario, "username" => $user,
        "nome" => $nome, "permissao" => $perm, "chave" => $chave, "ip" => $ip, "hora" => $hora);
//    $dao->gravaLog($usuario[0]['id'], Utils::$LOG_INFO, "Usuário logou no sistema.");
    // redirecionando para a pagina registrada.
//    session_regenerate_id(true);
//    session_write_close();
    header("location: ../index.php");
    exit();
} else {
    // redirecionando para o formulario de login com o erro.
    header("location: ../login.php?erro=1");
}
?>