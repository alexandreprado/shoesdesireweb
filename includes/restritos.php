<?php
//$path = get_complete_path();
//$configSite = "$path/includes/funcoes.php";
//include ("$configSite");

if (!isset($_SESSION)) {
    session_start();
}

if (isset($_SESSION['login'])) {
    $id_usuario = $_SESSION['login']["id"];
    $user = $_SESSION['login']["username"];
    $nome = $_SESSION['login']["nome"];
    $perm = $_SESSION['login']["permissao"];
    $login = $_SESSION['login']['username'];
    $hora = $_SESSION['login']['hora'];

    $secret = md5("ThereIsASecretInsideAndNobodyKnowsThat"); // chave secreta
    $ip = $_SESSION['login']['ip']; // pegando ip do usuario
    if ($_SESSION['login']['chave'] != md5($id_usuario . $user . $nome . $perm . $secret . $ip . $hora)) {
        // verificado se a chave é válida
        header("Location: login.php?erro=1");
    }

    // atualizando a chave com novo horario de acesso
    $hora = time();
    $chave = md5($id_usuario . $user . $nome . $perm . $secret . $ip . $hora);
    // registrando os novos dados na session.
    $_SESSION['login'] = array("id" => $id_usuario, "username" => $user,
        "nome" => $nome, "permissao" => $perm, "chave" => $chave, "ip" => $ip, "hora" => $hora);
} else {
    header("Location: " . get_path() . "/login.php");
    exit();
}

$browser = get_navegador();

if ($browser === "Internet Explorer 6" || $browser === "Internet Explorer 7") {
    header("Location: " . get_path() . "/login.php");
    exit();
}
?>