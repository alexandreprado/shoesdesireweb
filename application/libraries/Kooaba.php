<?php

class Kooaba {
# setup config variables

    private $data_key = "d2J3CmySiRiBCcFZ0awCFeZ06yuzPdZhp1tYUdqp";
    private $bucket_uuid = "2098c5d9-9a33-451f-bbd5-714f4e428143";
    private $url = "";

    public function __construct() {
        $this->url = "https://upload-api.kooaba.com/api/v4/buckets/" . $this->bucket_uuid . "/items";
    }

    public function do_upload($file_path, $file_name, $title, $metadata) {

        # Image to update
//        $file_path = "../images/db_image.jpg";
//        $file_name = "db_image.jpg";
//        $title = "An image";
//        $metadata = "null";
        $enabled = "true";
//        $reference_id = "r376466";

        if (file_exists($file_path)) {
            $img = file_get_contents($file_path);
        } else {
            die($file_path . ": File does not exist");
        }

        # Define boundary for multipart message
        $boundary = uniqid();

        # Construct the body of the request
        $body = $this->image_part($boundary, "images", $file_name, $img);
        $body .= $this->text_part($boundary, "title", $title);
        $body .= $this->text_part($boundary, "metadata", $metadata);
//        $body .= $this->text_part($boundary, "reference_id", $reference_id);
        $body .= $this->text_part($boundary, "enabled", $enabled);
        $body .= "--" . $boundary . "--\r\n";

        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: multipart/form-data; boundary=' . $boundary . "\r\n" .
                'Authorization: Token ' . $this->data_key,
                'content' => $body
            )
        ));

        $result = file_get_contents($this->url, false, $context);

        $resp = TRUE;

        if ($http_response_header[0] !== "HTTP/1.1 201 Created") {
            $resp = FALSE;
        }

        $response = array("result" => $resp, "data" => json_decode($result));

        return json_encode($response);
    }

    # add image part to a kooaba multipart request
    function image_part($boundary, $attr_name, $file_name, $data) {
        $str = "--" . $boundary . "\r\n";
        $str .= 'Content-Disposition: form-data; name="' . $attr_name . '"; filename="' . $file_name . '"' . "\r\n";
        $str .= 'Content-Transfer-Encoding: binary' . "\r\n";
        $str .= 'Content-Type: image/jpeg' . "\r\n\r\n";
        $str .= $data . "\r\n";
        return $str;
    }

    # add text part to a kooaba multipart request
    function text_part($boundary, $attr_name, $data) {
        $str = "--" . $boundary . "\r\n";
        $str .= 'Content-Disposition: form-data; name="' . $attr_name . '"' . "\r\n\r\n";
        $str .= $data . "\r\n";
        return $str;
    }

}

?>