<?php ?>

<style>
    .holder{
        margin: 0px;
        padding: 0px;
        margin: 40px 20px 20px 50px;
    }

    .users_holder{
        width: 300px;
        max-height: 500px;
        border: 1px solid gray;
        background-color: white;
        padding: 10px;
        margin-right: 20px;
    }

    .posts_holder{
        max-width: 500px;
        max-height: 600px;
        overflow: auto;
        border: 1px solid gray;
        background-color: white;
        padding: 10px;
    }

    .user{
        cursor: pointer;
        width: 100%;
    }

    .user:hover{
        background-color: darkblue;
        color: white;
    }

    .post{
        cursor: pointer;
        width: 100%;
    }

    .post:hover{
        background-color: darkorange;
        color: white;
    }

    .post_parent_holder{
        left: 0px;
        display: none;
    }

    .bt{
        width: 100px;
        height: 30px;
        line-height: 30px;
        color: white;
        background-color: darkmagenta;
        float: left;
        text-align: center;
        margin-right: 20px;
    }

    .search_posts, .search_users{
        font-style: italic;
        color: darkgray;
    }

    .loader_holder{
        position: absolute;
        margin-top: 90px;
        margin-left: 350px;
    }

    .user_active{
        background-color: darkblue;
        color: white;
    }

    .post_active{
        background-color: darkorange;
        color: white;
    }

    .paginacao{

    }

    .page{
        margin: 5px;
    }


</style>

<script>
    $(document).ready(function(){
        $("#user_id").chosen().trigger("liszt:updated");
        
        var posts = <?php echo sizeof($posts); ?>;
        
        if (posts > 0){
            $(".post_parent_holder").show();
        }
        
        $(".user").click(function(){
            
            $(".user").removeClass("user_active");
            
            $(this).addClass("user_active");
            
            $(".post_parent_holder").hide('slide', {direction: 'left'}, 300, function(){
                $(".loader_holder").fadeIn(300);
            });
            
            var user_id = $(this).attr("id");
           
            $.ajax({
                url: "<?php echo base_url() . "shoes/getPosts/"; ?>" + user_id,
                type: "GET",
                dataType: "JSON",
                success: function(data){
                    $(".loader_holder").fadeOut(300, function(){
                        $(".post_parent_holder").show('slide', {direction: 'left'}, 300);
                    });
                    
                    $(".posts_holder").empty();
                    
                    if (data.posts.length === 0){
                        $(".posts_holder").append("<p>No matches found</p>");
                    }
                    
                    for(var i = 0; i < data.posts.length; i++){
                        if (data.posts[i].Shoes !== undefined){
                            $(".posts_holder").append("<p class='post' id='" + data.posts[i].objectId + "'>" + data.posts[i].Shoes.model + " / " + data.posts[i].Shoes.brand + "<input type='hidden' class='brand' value='" + data.posts[i].Shoes.model + " / " + data.posts[i].Shoes.brand + "' /></p>");
                        }
                    }
                    
                    var total_pages = data.total_pages;
                    var page = data.page;
                    
                    var string = "";
                    
                    for (var i = 0; i < total_pages; i++){
                        string += "<a href='<?php echo base_url() . "shoes/shoes/"; ?>" + user_id + "/" + (i + 1) + "'><div class='page esq'>" + (i + 1) + "</div></a>";
                    }
                    
                    $(".paginacao").html(string);
                    
                    console.log(data.posts.length);
                }
            })
        });
        
        $(".search_users").click(function(){
            if ($(this).val() === "Search"){
                $(this).val("");
            }
        });
        
        $(".search_users").blur(function(){
            if ($(this).val() === ""){
                $(this).val("Search");
            }
        });
        
        $(".search_posts").click(function(){
            if ($(this).val() === "Search"){
                $(this).val("");
            }
        });
        
        $(".search_posts").blur(function(){
            if ($(this).val() === ""){
                $(this).val("Search");
            }
        });
        
        $(".search_posts").keyup(function() {

            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val(), count = 0;

            // Loop through the comment list
            $(".post").each(function() {

                // If the list item does not contain the text phrase fade it out
                if ($(this).find(".brand").val().search(new RegExp(filter, "i")) < 0) {
                    $(this).fadeOut();

                    // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $(this).show();
                    count++;
                }
            });
        });
        
        $(".search_users").keyup(function() {

            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val(), count = 0;

            // Loop through the comment list
            $(".user").each(function() {

                // If the list item does not contain the text phrase fade it out
                if ($(this).html().search(new RegExp(filter, "i")) < 0) {
                    $(this).fadeOut();

                    // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $(this).show();
                    count++;
                }
            });
        });
        
        $(".post").live("click", function(){
            var post_id = $(this).attr("id");
            window.location.href = "<?php echo base_url(); ?>shoes/editar/" + post_id;
        });
    });
</script>

<div class="esq holder">

    <!--    <div class="esq">-->
    <a class="bt" style="margin-bottom: 20px;" href="<?php echo base_url() . "shoes/novo"; ?>">New Shoes</a>

    <a class="bt" style="margin-bottom: 20px;" href="<?php echo base_url() . "related"; ?>">Related</a>
    <!--    </div>-->

    <div class="clear"></div>

    <div class="esq">

        <span style="font-style: italic; float: left">Users</span>

        <div class="clear"></div>

        <div class="roundBordersP_2px esq" style="margin-bottom: 15px; margin-top: 10px">
            <input type="text" class="search_users" value="Search" />
        </div>

        <div class="clear"></div>

        <div class="esq users_holder shadow">
            <?php foreach ($users as $user) { ?>
                <p id="<?php echo $user->objectId; ?>" class="user <?php echo $user->objectId === $userId ? "user_active" : ""; ?>"><?php echo $user->username; ?></p>
            <?php } ?>
        </div>

    </div>

    <div class="esq post_parent_holder">

        <span style="font-style: italic; float: left">Posts</span>

        <div class="clear"></div>

        <div class="roundBordersP_2px esq" style="margin-bottom: 15px; margin-top: 10px">
            <input type="text" class="search_posts" value="Search" />
        </div>

        <div class="clear"></div>

        <div class="esq posts_parent_holder">
            <div class="paginacao esq">
                <?php for ($i = 0; $i < $total_pages; $i++) { ?>
                    <a href="<?php echo base_url() . "shoes/shoes/$userId/" . ($i + 1); ?>"><div class="page esq"><?php echo ($i + 1); ?></div></a>
                <?php } ?>
            </div>

            <div class="clear"></div>

            <div class="esq posts_holder shadow">
                <?php foreach ($posts as $post) { ?>
                    <?php $model = property_exists($post->Shoes, "model") ? $post->Shoes->model : ""; ?>
                    <?php $brand = property_exists($post->Shoes, "brand") ? $post->Shoes->brand : ""; ?>
                    <p class='post' id='<?php echo $post->objectId; ?>'><?php echo $model . " / " . $brand; ?><input type='hidden' class='brand' value='<?php echo $model . " / " . $brand; ?>' /></p>
                <?php } ?>
            </div>

        </div>
    </div>

    <div class="loader_holder esq" style="display: none">
        <img src="<?php echo relative_url() . $images . "shoes_loader.gif" ?>" />
    </div>

    <?php // echo form_dropdown("user_id", $users, set_value("user_id"), "id=user_id"); ?>
</div>

