<?php if ($error) { ?>
    <div id="erro_operacao"><span><?php echo $error; ?></span></div>
<?php } ?>

<script>
    Parse.initialize("QsGVQvURfVQn7PIxfJIMSIMgKaIxlZXmJ7EbStYZ", "D8NlLLo2qvP100kc4Oa2rB5hang08gx5KWJ9bwRS");

    $(document).ready(function() {

        var x;
        var y;
        var w;
        var h;
        
        var venue_ = "<?php echo set_value('store', ""); ?>";
        var city_ = "<?php echo set_value('city', ""); ?>";
        var state_ = "<?php echo set_value('state', ""); ?>";
        
        if (venue_ !== "" && city_ !== "" && state_ != ""){
            fillVenues(venue_, city_, state_);
        }

        $(function() {
            $.mask.addPlaceholder("~", "[+-]");
            $("#cpf").mask("999.999.999-99");
            $(".telefones").mask("(99) 9999-9999");
            $(".cep").mask("99.999-999");
            $(".dates").mask("99/99/9999");
        });

        $('.money').priceFormat({
            prefix: '$',
            centsSeparator: '.',
            thousandsSeparator: ','
        });
        
        $("#user_id").chosen().trigger("liszt:updated");
        $("#category_id").chosen().trigger("liszt:updated");
        $("#venue_id").chosen().trigger("liszt:updated");

        $("#dt_nascimento").datepicker({
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
            dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            nextText: 'Próximo',
            prevText: 'Anterior',
            changeMonth: true,
            changeYear: true,
            yearRange: "-110:+0"
        });

        $("#upload").hover(function() {
            $(".photo_mask").slideDown(200);
            $(".photo_mask_back").slideDown(200);
        }, function() {
            $(".photo_mask").slideUp(200);
            $(".photo_mask_back").slideUp(200);
        });

        $(function() {
            $('#upload').fileupload({
                dataType: 'json',
                url: '<?php echo relative_url() . "shoes/upload"; ?>',
                type: 'POST',
                progress: function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    if (progress > 0) {
                        $(".ui-progress").fadeIn(100);
                    }
                    $(".ui-progress").css({"width": progress + "%"});
                    $(".ui-label .value").html(progress + "%");
                },
                add: function(e, data) {
                    $(".comment").hide(0);
                    $(".ui-label .value").html("0%");
                    $(".ui-progress").css({"width": "0%"});
                    $(".upload_bar_holder").fadeIn(300);
                    data.submit();
                },
                done: function(e, data) {

                    if (data.result.result) {

                        $("#cropbox").remove();
                        $("#preview").remove();
                        $("#options").empty();
                        $("#preview_container").empty();
                        $("#image_container").empty();

                        $("#crop_holder").css({"width": (data.result.image_x + 180)}).css({"height": (data.result.image_y + 20)});

                        var img = new Image();
                        $(img).load(function() {
                            $imgpos.width = data.result.image_x;
                            $imgpos.height = data.result.image_y;
                            this.width = data.result.image_x;
                            this.height = data.result.image_y;
                            $(this).attr('id', 'cropbox');
                            $(this).hide();
                            $("#image_container").empty();
                            $('#image_container').append(this);

                            $(this).fadeIn().Jcrop({
                                onChange: showPreview,
                                onSelect: showPreview,
                                aspectRatio: 1,
                                onSelect: updateCoords,
                                setSelect: [0, 0, 320, 320]
                            });
                            
                            // ****************Início lógica que define o crop inicial
                            var img_x = data.result.image_x;
                            var img_y = data.result.image_y;

                            if (data.result.image_x > 320) {
                                img_x = 320;
                            }
                            if (data.result.image_y > 320) {
                                img_y = 320;
                            }

                            if (img_x > img_y) {
                                img_x = img_y;
                            } else if (img_x < img_y) {
                                img_y = img_x;
                            }
                            
                            updateCoords({x: 0, y: 0, w: img_x, h: img_y});
                            // ****************Fim lógica

                            $("#preview_container").empty();

                            var _imgprev = $(document.createElement('img')).attr('id', 'preview').attr('src', "<?php echo relative_url() . $uploads; ?>" + data.result.image);
                            $('#preview_container').append(_imgprev);

                            var _crop_bttn = $(document.createElement('div')).attr('class', 'crop_btn roundBordersP_5px shadow_light').append('<span>Salvar</span>');
                            var _crop_cancel_bttn = $(document.createElement('div')).attr('class', 'crop_cancel_btn roundBordersP_5px shadow_light').append('<span>Cancelar</span>');
                            _crop_cancel_bttn.click(function() {
                                $.colorbox.close();
                            });
                            _crop_bttn.click(function() {

                                $.colorbox.close();
                                $(".photo_mask").slideUp(200);
                                $(".photo_mask_back").slideUp(200);

                                $(".loading_circle_holder").fadeIn(200);

                                // ******** ENVIANDO AO CROP ***********
                                $.ajax({
                                    dataType: 'json',
                                    type: 'POST',
                                    url: '<?php echo relative_url(); ?>shoes/crop',
                                    data: {
                                        tempfile: '<?php echo $uploads; ?>' + data.result.image,
                                        "<?php echo $this->config->item('csrf_token_name'); ?>": $.cookie("csrf_cookie_name"),
                                        image_x: x,
                                        image_y: y,
                                        image_w: w,
                                        image_h: h
                                    },
                                    success: function(response) {

                                        if (!response.result) {
                                            $(".loading_circle_holder").fadeOut(300);
                                            alert(response.error);
                                        } else {
                                            
                                            var d = new Date();
                                            var date = d.getTime();

                                            // ******** ENVIANDO AO KOOABA ***********
                                            $.ajax({
                                                dataType: 'json',
                                                type: 'POST',
                                                url: '<?php echo relative_url(); ?>shoes/sendKooaba',
                                                data: {
                                                    file_path: '<?php echo complete_url() . $uploads; ?>' + data.result.image,//"<?php //echo complete_url() . $uploads; ?>" + response.image,
                                                    file_name: response.image,
                                                    title: "image_" + date,
                                                    "<?php echo $this->config->item('csrf_token_name'); ?>": $.cookie("csrf_cookie_name")
                                                },
                                                beforeSend: function() {
                                                    console.log("Aguarde");
                                                    console.log("Imagem: " + '<?php echo complete_url() . $uploads; ?>' + data.result.image);
                                                },
                                                success: function(result) {

                                                    if (result.result) {

                                                        console.log("UUID: " + result.data.uuid);

                                                        // ******** ENVIANDO AO PARSE ***********     

                                                        // Transforma a imagem em uma string base64 para poder enviar ao Parse
                                                        var img = new Image();
                                                        img.src = "<?php echo base_url() . $uploads; ?>" + response.image;
                                                        img.onload = function() {

                                                            var canvas = document.createElement("canvas");
                                                            canvas.width = this.width;
                                                            canvas.height = this.height;

                                                            var ctx = canvas.getContext("2d");
                                                            ctx.drawImage(this, 0, 0);

                                                            var dataURL = canvas.toDataURL("image/png");
                                                            var file64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
                                                            var parse = new Parse.File(response.image, {base64: file64});

                                                            // Salva o Parse.File
                                                            parse.save().then(function(obj) {

                                                                console.log("Caminho imagem parse: " + obj._url + ", nome imagem parse: " + obj._name);
                                                                $("#user_picture").load(function() {
                                                                    $(".loading_circle_holder").fadeOut(300);
                                                                }).attr("src", "<?php echo relative_url() . $uploads; ?>" + response.image);

                                                                //Avisa que é pra alterar a foto
                                                                $("#update_photo").val(true);
                                                                $("#image_name").val(obj._name);
                                                                $("#image_local").val(response.image);
                                                                $("#image_url").val(obj._url);
                                                                $("#kooaba_uuid").val(result.data.uuid);
                                                            }, function(error) {
                                                                console.log(error);
                                                                $(".loading_circle_holder").fadeOut(300);
                                                                alert("Ocorreu um erro ao salvar o arquivo. Tente novamente mais tarde.");
                                                            });
                                                        };
                                                    } else {
                                                        $(".comment").fadeIn(100);
                                                        alert(result.data);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            });
                            $('#options').append(_crop_bttn).append(_crop_cancel_bttn);
                            $.colorbox({href: "#crop_holder", inline: true});

                            $(".ui-progress").fadeOut(100, function() {
                                $(".comment").fadeIn(100);
                            });
                            $(".upload_bar_holder").fadeOut(100);
                            $(".ui-progress").css({"width": "0%"});
                            $(".ui-label .value").html("0%");

                        }).attr('src', "<?php echo relative_url() . $uploads; ?>" + data.result.image);

                    } else {

                        $(".ui-progress").fadeOut(100);
                        $(".upload_bar_holder").fadeOut(100);
                        $(".ui-progress").css({"width": "0%"});
                        $(".ui-label .value").html("0%");
                        $(".comment").fadeIn(100);

                        alert(data.result.error);
                    }
                }
            });
        });

        function updateCoords(c) {
            x = c.x;
            y = c.y;
            w = c.w;
            h = c.h;
            console.log("x: " + x + ", y: " + y + ", w: " + w + ", h: " + h);
        }

        function checkCoords() {

            if (parseInt(w)) {
                return true;
            }
            x = 0;
            y = 0;
            w = 320;
            h = 320;
            return true;
        }

        function showPreview(coords) {

            if (parseInt(coords.w) > 0) {
                var rx = 92 / coords.w;
                var ry = 92 / coords.h;
                $('#preview').css({
                    width: Math.round(rx * $imgpos.width) + 'px',
                    height: Math.round(ry * $imgpos.height) + 'px',
                    marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                    marginTop: '-' + Math.round(ry * coords.y) + 'px'
                });
            }
        }

        $imgpos = {
            width: '92',
            height: '92'
        };
        
        function fillVenues(venue, city, state){
            $.ajax({
                type: 'GET',
                url: '<?php echo relative_url(); ?>shoes/lookForVenues/' + venue + '/' + city + '/' + state,
                beforeSend: function(){
                    $("#venue_id").html("<option>Loading...</option>");
                    $("#venue_id").chosen().trigger("liszt:updated");
                },
                success: function(result){
                    $("#venue_id").html(result);
                    $("#venue_id").val("<?php echo set_value("venue_id"); ?>");
                    $("#venue_id").chosen().trigger("liszt:updated");
                }
            });
        }

        $(".search_fs").click(function(){
            
            var venue = $("#store").val();
            var city = $("#city").val();
            var state = $("#state").val();
            
            if (venue !== "" && city !== "" && state != ""){
                fillVenues(venue, city, state);
            } else {
                alert("Fill in all required fields");
            }
        });
        
        $("#pricing li").click(function() {
            var classe = $(this).attr("class");
            $(this).parent().removeAttr("class").addClass("rating").addClass(classe + "star");
            $("#price").val($(this).index() + 1);
        });

        $("#rate li").click(function() {
            var classe = $(this).attr("class");
            $(this).parent().removeAttr("class").addClass("rating").addClass(classe + "star");
            $("#rating").val($(this).index() + 1);
        });
    });
</script>

<style>
    /* star rating code - use lists because its more semantic */
    /* No javascript required */
    /* all the stars are contained in one matrix to solve rollover problems with delay */
    /* the background position is just shifted to reveal the correct image. */
    /* the images are 16px by 16px and the background position will be shifted in negative 16px increments */
    /*  key:  B=Blank : O=Orange : G = Green * /
    /*..... The Matrix ....... */
    /* colours ....Background position */
    /* B B B B B - (0 0)*/
    /* G B B B B - (0 -16px)*/
    /* G G B B B - (0 -32px)*/
    /* G G G B B - (0 -48px)*/
    /* G G G G B - (0 -64px)*/
    /* G G G G G - (0 -80px)*/
    /* O B B B B - (0 -96px)*/
    /* O O B B B - (0 -112px)*/
    /* O O O B B - (0 -128px)*/
    /* O O O O B - (0 -144px)*/
    /* O O O O O - (0 -160px)*/

    /* the default rating is placed as a background image in the ul */
    /* use the background position according to the table above to display the required images*/
    .rating{
        /*height: 40px;*/
        width: 422px;
        background: #ffffff;
        /*border: 1px solid #e7e6e6;*/
        /*margin-bottom: 20px;*/
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        float: left;

        /*width:80px;*/
        height:16px;
        margin:12px 0px 30px 5px;
        padding:0;
        list-style:none;
        /*clear:both;*/
        position:relative;
        background: url(<?php echo relative_url() . $images; ?>star-matrix.gif) no-repeat 0 0;
    }
    /* add these classes to the ul to effect the change to the correct number of stars */
    .nostar {background-position:0 0}
    .onestar {background-position:0 -16px}
    .twostar {background-position:0 -32px}
    .threestar {background-position:0 -48px}
    .fourstar {background-position:0 -64px}
    .fivestar {background-position:0 -80px}
    ul.rating li {
        cursor: pointer;
        /*ie5 mac doesn't like it if the list is floated\*/
        float:left;
        /* end hide*/
        text-indent:-999em;
    }
    ul.rating li a {
        position:absolute;
        left:0;
        top:0;
        width:16px;
        height:16px;
        text-decoration:none;
        z-index: 200;
    }
    ul.rating li.one a {left:0}
    ul.rating li.two a {left:16px;}
    ul.rating li.three a {left:32px;}
    ul.rating li.four a {left:48px;}
    ul.rating li.five a {left:64px;}
    ul.rating li a:hover {
        z-index:2;
        width:80px;
        height:16px;
        overflow:hidden;
        left:0;	
        background: url(<?php echo relative_url() . $images; ?>star-matrix.gif) no-repeat 0 0
    }
    ul.rating li.one a:hover {background-position:0 -96px;}
    ul.rating li.two a:hover {background-position:0 -112px;}
    ul.rating li.three a:hover {background-position:0 -128px}
    ul.rating li.four a:hover {background-position:0 -144px}
    ul.rating li.five a:hover {background-position:0 -160px}

    /* end rating code */
    h3{margin:0 0 2px 0;font-size:110%}

    textarea{
        border: none;
        outline: none;
    }

    fieldset{
        border: 1px solid hotpink;
        margin-left: 84px;
        padding-top: 20px;
        padding-bottom: 10px;
        margin-bottom: 10px;
    }

    fieldset label{
        width: 4.1em !important;
    }

    legend{
        margin-left: 16px;
    }

    .search_fs{
        width: 70px;
        height: 30px;
        text-align: center;
        line-height: 30px;
        background-color: red;
        cursor: pointer;
        margin-left: 63px;
        margin-bottom: 10px;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
    }

    .search_fs span{
        color: white;
        font-size: 14px;
    }

</style>

<div class="form_holder">
    <?php echo form_open(base_url() . "shoes/novo", array("method" => "post", "id" => "form", "class" => "esq")); ?>
    <div class="clear"></div>
    <div class="inner_form_clientes esq">
        <div class="esq"> 

            <span style="position: absolute; margin-left: 130px">*</span> 

            <div class="picture_holder esq shadow_light">
                <?php echo form_error('image_name'); ?>
                <div class="esq">

                    <div class="div_input_file">

                        <?php $img = set_value('image_local'); ?>
                        <img src="<?php echo $img ? base_url() . $uploads . $img : base_url() . $images . 'shoes.png'; ?>" class="esq" id="user_picture" />
                        <input type="hidden" name="image_local" id="image_local" value="<?php echo set_value("image_local"); ?>" />
                        <input type="hidden" name="image_url" id="image_url" value="<?php echo set_value("image_url"); ?>"/>
                        <input type="hidden" name="image_name" id="image_name" value="<?php echo set_value("image_name"); ?>" />
                        <input type="hidden" name="kooaba_uuid" id="kooaba_uuid" value="<?php echo set_value("kooaba_uuid"); ?>" />

                        <input id="upload" type="file" name="uploadfile" />

                        <div class="photo_mask"></div>
                        <div class="photo_mask_back"><span>Change</span></div>

                        <div class="loading_circle_holder">
                            <img src="<?php echo base_url() . $images . 'loading_circle.gif'; ?>" />
                        </div>
                    </div>
                </div>

            </div>

            <div class="upload_bar_holder dir" style="display: none">
                <div class="ui-progress-bar ui-container" id="progress_bar">

                    <div class="ui-progress" style="width: 0%; display: none"></div>
                    <span class="ui-label">
                        <b class="value">0%</b>
                    </span>
                </div>
            </div>
        </div>

        <div class="esq comment">
            <span style="margin-left: 22px; font-size: 14px">Add a Comment</span>
            <div class="clear"></div>
            <div class="textarea_holder">
                <textarea cols="28" rows="4" name="comment"><?php echo set_value("comment"); ?></textarea>
            </div>
        </div>

        <div class="clear"></div>

        <div class="esq">
            <label for="user_id">* User:</label>
            <div class="select_holder">
                <?php echo form_error('user_id'); ?>
                <?php echo form_dropdown("user_id", $users, set_value("user_id"), "id=user_id"); ?>
            </div>
        </div>

        <div class="esq">
            <label for="category_id">* Category:</label>
            <div class="select_holder">
                <?php echo form_error('category_id'); ?>
                <?php echo form_dropdown("category_id", $categories, set_value("category_id"), "id=category_id"); ?>
            </div>
        </div>

        <div class="esq">
            <label for="brand">* Brand:</label>
            <div class="input_holder">
                <?php echo form_error('brand'); ?>
                <input type="text" name="brand" id="brand" value="<?php echo set_value('brand'); ?>" />
            </div>
        </div>

        <div class="esq">
            <label for="model">* Model:</label>
            <div class="input_holder">
                <?php echo form_error('model'); ?>
                <input type="text" name="model" id="model" value="<?php echo set_value('model'); ?>" />
            </div>
        </div>

        <!--        <div class="esq">
                    <label for="price">Price:</label>
                    <div class="input_holder">
        <?php // echo form_error('price'); ?>
                        <input type="text" name="price" class="money" id="price" value="<?php // echo set_value('price');                ?>" />
                    </div>
                </div>-->

        <div class="esq">
            <fieldset>
                <legend>Venue</legend>
                <label for="store">Name:</label>
                <div class="input_holder">
                    <?php echo form_error('store'); ?>
                    <input type="text" name="store" id="store" value="<?php echo set_value('store', 'Steve Madden'); ?>" />
                </div>
                <label for="city">City:</label>
                <div class="input_holder">
                    <?php echo form_error('city'); ?>
                    <input type="text" name="city" id="city" value="<?php echo set_value('city'); ?>" />
                </div>
                <label for="state">State:</label>
                <div class="input_holder">
                    <?php echo form_error('state'); ?>
                    <input type="text" name="state" id="state" value="<?php echo set_value('state'); ?>" maxlength="2" />
                </div>

                <div class="esq">
                    <div class="search_fs">
                        <span>Search</span>
                    </div>
                </div>

                <div class="clear"></div>

                <label for="venue_id">Store:</label>
                <div class="select_holder">
                    <?php echo form_error('venue_id'); ?>
                    <?php echo form_dropdown("venue_id", array(), set_value("venue_id"), "id=venue_id"); ?>
                </div>
            </fieldset>
        </div>

        <div class="esq">
            <label for="price">Price:</label>

            <?php
            $clazz = set_value('price', 0);

            switch ($clazz) {
                case 0:
                    $clazz = "nostar";
                    break;
                case 1:
                    $clazz = "onestar";
                    break;
                case 2:
                    $clazz = "twostar";
                    break;
                case 3:
                    $clazz = "threestar";
                    break;
                case 4:
                    $clazz = "fourstar";
                    break;
                case 5:
                    $clazz = "fivestar";
                    break;
                default:
                    $clazz = "nostar";
                    break;
            }
            ?>

            <ul class="rating price <?php echo $clazz; ?>" id="pricing">
                <?php echo form_error('price'); ?>
                <li class="one"><a href="javascript:void(0)" title="1 Star">1</a></li>
                <li class="two"><a href="javascript:void(0)" title="2 Stars">2</a></li>
                <li class="three"><a href="javascript:void(0)" title="3 Stars">3</a></li>
                <li class="four"><a href="javascript:void(0)" title="4 Stars">4</a></li>
                <li class="five"><a href="javascript:void(0)" title="5 Stars">5</a></li>
                <input type="hidden" name="price" id="price" value="<?php echo set_value('price'); ?>" />
            </ul>
        </div>

        <div class="esq">
            <label for="rating">Rating:</label>

            <?php
            $class = set_value('rating', 0);

            switch ($class) {
                case 0:
                    $class = "nostar";
                    break;
                case 1:
                    $class = "onestar";
                    break;
                case 2:
                    $class = "twostar";
                    break;
                case 3:
                    $class = "threestar";
                    break;
                case 4:
                    $class = "fourstar";
                    break;
                case 5:
                    $class = "fivestar";
                    break;
                default:
                    $class = "nostar";
                    break;
            }
            ?>

            <ul class="rating <?php echo $class; ?>" id="rate">
                <?php echo form_error('rating'); ?>
                <li class="one"><a href="javascript:void(0)" title="1 Star">1</a></li>
                <li class="two"><a href="javascript:void(0)" title="2 Stars">2</a></li>
                <li class="three"><a href="javascript:void(0)" title="3 Stars">3</a></li>
                <li class="four"><a href="javascript:void(0)" title="4 Stars">4</a></li>
                <li class="five"><a href="javascript:void(0)" title="5 Stars">5</a></li>
                <input type="hidden" name="rating" id="rating" value="<?php echo set_value('rating'); ?>" />
            </ul>
        </div>
    </div>

    <input type="hidden" name="acao" value="salvar" />

    <div class="clear"></div>

    <div class="dir" style="margin-right: 60px">
        <!--<div class="controls_holder shadow">-->
        <input type="submit" class="submit_bt" value="" title="Save"/>
        <input type="button" class="cancel_bt" value="" title="Cancel" onclick="location.href = '<?php echo base_url() . "shoes"; ?>'"/>
        <!--</div>-->
    </div>

    <div class="esq" style="margin-left: 85px;font-size: 12px;margin-top: 25px;">
        <span>(*) - Required fields</span>
    </div>

    <?php echo form_close(); ?>
</div>
<div style="display: none">
    <div class="crop_holder esq" id="crop_holder">
        <div id="preview_border" class="shadow2 roundBordersP_5px"><div id="preview_container" class="esq"></div></div>
        <div id="image_border" class="shadow2 roundBordersP_5px"><div id="image_container" class="esq"></div></div>
        <div id="options" class="esq"></div>
    </div>
</div>
