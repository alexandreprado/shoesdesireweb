<?php
$error = $this->session->flashdata('error');
$success = $this->session->flashdata('success');

echo doctype('xhtml1-trans');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shoes D-Sire Admin</title>

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/960.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/padrao.css"/>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.9.0.min.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.8.3.js" ></script>
<!--        <script type="text/javascript" src="<?php echo base_url(); ?>js/datatable/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/datatable/media/js/dataTables.date-euro.js"></script>-->

        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.10.0.custom.js" ></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui-1.10.0.custom.css"/>

<!--        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery_rotate.js" ></script>-->

        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/maskedinput-1.1.2.pack.js"></script>

<!--        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.timepicker.js"></script>-->
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery.timepicker.css"/>-->

        <script src="<?php echo base_url(); ?>js/ajaxupload.3.5.js" type="text/javascript"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/colorbox/jquery.colorbox.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/colorbox/theme5/colorbox.css" />

        <script src="<?php echo base_url(); ?>js/jcrop/jquery.Jcrop.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/jcrop/jquery.Jcrop.css" type="text/css" />

        <script type="text/javascript" src="<?php echo base_url(); ?>js/chosen/chosen.jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/chosen/chosen.css" />

        <script src="<?php echo base_url(); ?>js/jquery.fileupload.js"></script>

        <script src="<?php echo base_url(); ?>js/jquery.cookie.js"></script>

        <script type="text/javascript" src="http://www.parsecdn.com/js/parse-1.2.7.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/price_format.js"></script>

        <script>
            $(document).ready(function() {

                if ($("#erro_operacao").is(":visible")){
                    setTimeout( function()  {
                        $("#erro_operacao").fadeOut(300); 
                    }, 3000); 
                }
                
                if ($("#sucesso_operacao").is(":visible")){
                    setTimeout( function()  {
                        $("#sucesso_operacao").fadeOut(300); 
                    }, 3000); 
                }
            });
        </script>

        <style>
            #erro_operacao, #sucesso_operacao{
                top: 0px !important;
            }
        </style>

    </head>
    <body>

        <!--        <div class="topo">
                    <div class="container_12">
                        <div class="logo esq"></div>
                        <div class="user_information_holder dir">
                            <img class="user_image dir" src="<?php // echo trim($user->imagem) ? base_url() . $this->config->item('uploads_usuarios') . $user->imagem : base_url() . $images . 'no-photo.png';      ?>" />
                            <div class="user_text_holder esq">
                                <span class="user_name dir"><?php // echo $user->tratamento . " " . ucwords($user->nome);      ?></span>
                                <div class="clear"></div>
                                <a href="logout"><span class="logout dir">logout</span></a>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class="breadcumb">
                    <div class="container_12">
                        <span><a href="<?php // echo base_url();      ?>">Home</a></span>
        <?php
//                $count = 1;
//                $url = base_url();
        ?>
        <?php // foreach ($segmentos as $segmento) { ?>
        <?php // $url .= "$segmento/"; ?>
        <?php // $last_bd = $count < sizeof($segmentos) ? FALSE : TRUE; ?>
                            <span> > </span>
                            <span <?php // echo $last_bd ? "class='bd_active'" : "";      ?>>
        <?php // echo $last_bd ? "" : "<a href='$url'>"; ?>
        <?php // echo ucfirst($segmento); ?>
        <?php // echo $last_bd ? "" : "</a>"; ?>
                            </span>
        <?php
//                    $count++;
//                }
        ?>
                    </div>
                </div>
        
        --> 
<!--        <div id="erro_operacao"><span><?php // echo $error;  ?></span></div>-->
        <?php if ($error) { ?>
            <div id="erro_operacao"><span><?php echo $error; ?></span></div>
        <?php } else if ($success) { ?>
            <div id="sucesso_operacao"><span><?php echo $success; ?></span></div>
        <?php } ?>

        <!--                <div class="content container_12">-->


