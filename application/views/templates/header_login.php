<?php
//$file = base_url()."/includes/configSite.php";
//include ("$file");
//$config = new configs;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JustDoctor</title>

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/960.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/padrao.css"/>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.9.0.min.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/datatable/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/datatable/media/js/dataTables.date-euro.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.10.0.custom.js" ></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui-1.10.0.custom.css"/>

    </head>
    <body>

        <div class="topo_login">
            <div class="container_12">
                <div class="logo_login"></div>
            </div>
        </div>

        <?php echo validation_errors(); ?>
        <?php // echo $config->getMensagemNavegador(); ?>

        <div class="content container_12">


