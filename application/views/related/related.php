<?php ?>

<style>

    body{
        background-color: #F0F0F0;
        margin: 0px;
        padding: 0px;
    }

    div{
        display: block;
    }

    .roundBordersP_2px{
        border-radius: 2px;
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
    }

    .roundBordersP_3px{
        border-radius: 3px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
    }

    .roundBordersP_4px{
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
    }

    .roundBordersP_5px{
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
    }

    .roundBordersP_6px{
        border-radius: 6px;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
    }

    .roundBordersP_8px{
        border-radius: 8px;
        -moz-border-radius: 8px;
        -webkit-border-radius: 8px;
    }

    .shadow_light {
        -moz-box-shadow: 0px 2px 3px -1px #b7b8b8;
        -webkit-box-shadow: 0px 2px 3px -1px #b7b8b8;
        box-shadow: 0px 2px 3px -1px #b7b8b8;
        /* For IE 8 */
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=115, Color='#b7b8b8')";
        /* For IE 5.5 - 7 */
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=115, Color='#b7b8b8');
    }

    .shadow {
        -moz-box-shadow: 0px 2px 5px 0px #b7b8b8;
        -webkit-box-shadow: 0px 2px 5px 0px #b7b8b8;
        box-shadow: 0px 2px 5px 0px #b7b8b8;
        /* For IE 8 */
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=115, Color='#b7b8b8')";
        /* For IE 5.5 - 7 */
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=115, Color='#b7b8b8');
    }

    .shadow2 {
        -moz-box-shadow: 1px 2px 2px 1px #b7b8b8;
        -webkit-box-shadow: 1px 2px 2px 1px #b7b8b8;
        box-shadow: 1px 2px 2px 1px #b7b8b8;
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=115, Color='#b7b8b8')";
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=115, Color='#b7b8b8');
    }

    .shadow3 {
        -moz-box-shadow: 1px 2px 3px 0px #b7b8b8;
        -webkit-box-shadow: 1px 2px 3px 0px #b7b8b8;
        box-shadow: 1px 2px 3px 0px #b7b8b8;
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=115, Color='#b7b8b8')";
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=115, Color='#b7b8b8');
    }

    .related_holder{
        margin: auto;
        width: 960px;
    }

    .related_to_holder{
        width: 420px;
        background-color: white;
        margin-top: 40px;
        padding: 20px;
    }

    .related_to_search{
        width: 320px;
        height: 40px;
        background-color: white;
        border: 1px solid #E0E0E0;
        margin-top: 5px;
        margin-bottom: 10px;
    }

    .related_to_images{
        width: 100%;
        height: 600px;
        border: 1px solid #E0E0E0;
        margin-top: 10px;
        overflow: auto;
    }

    .related_to_images img{
        float: left;
        cursor: pointer;
        opacity: 1;
    }

    .related_to_images img:hover{
        opacity: 0.6;
    }

    .imageWrap {
        position:relative;
        width: 90px;
        height: 90px;
        padding: 5px;
        background-color: white;
    }

    .ok_original, .ok_related {
        position: absolute;
        width: 38px;
        height: 38px;
        float: right;
        right: 5px;
        display: none;
        z-index: 100;
    }

    .search{
        border: none;
        outline: none;
        width: 200px;
        background: none;
        font-size: 12px;
        color: #b0afaf;
        font-style: italic;
        margin-top: 5px;
        margin-left: 10px;
        height: 30px;
    }

    .title {
        font-size: 20px;
        font-style: italic;
    }

    .bt{
        width: 100px;
        height: 30px;
        line-height: 30px;
        color: white;
        background-color: darkmagenta;
        float: left;
        text-align: center;
        margin-right: 20px;
    }

    .loader{
        margin-top: 100px;
        margin-left: 170px;
        display: none;
    }

</style>

<script>
    $(document).ready(function() {

        $(".original_image").live('click', function() {
            $(".ok_original").hide();
            var id = $(this).attr("id");
            $("#" + id + "_").show();
            $("#ori_image").val(id);
        });

        $(".related_image").live('click', function() {
            var id = $(this).attr("id");
            if ($("#" + id + "_").is(':visible')) {
                $("#" + id + "_").hide();
                var ar = id.split("_");
                removeRelImage(ar[0]);
                $("#checkAll").attr("checked", false);
            } else {
                $("#" + id + "_").show();
                var ar = id.split("_");
                addRelImage(ar[0]);
            }
        });

        $("#search_ori").keyup(function() {

            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val(), count = 0;

            // Loop through the comment list
            $(".ori_wrap").each(function() {

                // If the list item does not contain the text phrase fade it out
                if ($(this).find(".brand").val().search(new RegExp(filter, "i")) < 0) {
                    $(this).parent().fadeOut();

                    // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $(this).parent().show();
                    count++;
                }
            });
        });

        $("#search_rel").keyup(function() {

            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val(), count = 0;

            // Loop through the comment list
            $(".rel_wrap").each(function() {

                // If the list item does not contain the text phrase fade it out
                if ($(this).find(".brand").val().search(new RegExp(filter, "i")) < 0) {
                    $(this).parent().fadeOut();

                    // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $(this).parent().show();
                    count++;
                }
            });
        });

        $(".search").focus(function() {
            if ($(this).val().trim() === "Search") {
                $(this).val("");
            }
        });

        $(".search").blur(function() {
            if ($(this).val().trim() === "") {
                $(this).val("Search");
            }
        });

        function addRelImage(objectId) {
            $(".ids_holder").append("<input type='hidden' name='rel_images[]' id='" + objectId + "__' value='" + objectId + "' />");
        }

        function removeRelImage(objectId) {
            $("#" + objectId + "__").remove();
        }
        
        function removeAllRelImages() {
            $(".ids_holder").empty();
        }
        
        $("#checkAll").live('click', function(){
            
            if ($(this).is(':checked')){
                $(".ok_related").show();
                
                removeAllRelImages();
                
                $(".related_image").each(function(){
                    if ($(this).is(":visible")){
                        var id = $(this).attr("id");
                        var ar = id.split("_");
                        addRelImage(ar[0]);
                    }
                });
            } else {
                
                $(".related_image").each(function(){
                    if ($(this).is(":visible")){
                        var id = $(this).attr("id");
                        var ar = id.split("_");
                        removeRelImage(ar[0]);
                    }
                });
                
                $(".ok_related").hide();
            }
        });
        
        $(".category_1").change(function(){
        
            var id = $(this).val();
            $("#related_to_images_1").empty();
            $(".loader_1").fadeIn(300);
            
            $.ajax({
                url: "<?php echo base_url() . "related/getPosts/"; ?>" + id,
                type: "GET",
                dataType: "json",
                success: function(data){
                    
                    $(".loader_1").fadeOut(300);
                   
                    for(var i = 0; i < data.length; i++){

                        if (data[i].Shoes !== undefined){
                            $("#related_to_images_1").append("<div class='imageWrap esq'>" +
                                "<div class='ori_wrap'>" +
                                "<img id='" + data[i].Shoes.objectId + "_' class='ok_original' src='<?php echo relative_url() . $images . "button_ok"; ?>' />" +
                                "<img id='" + data[i].Shoes.objectId + "' class='original_image' src='" + data[i].Shoes.image.url + "' width='90' height='90' title='" + data[i].Shoes.category.description + " | " + data[i].Shoes.model + "/" + data[i].Shoes.brand + "' />" +
                                "<input type='hidden' class='brand' value='" + data[i].Shoes.category.description + " | " + data[i].Shoes.model + "/" + data[i].Shoes.brand + "' />" +
                                "</div>" +
                                "</div>");
                        }
                    }
                }
            });
        });
        
        $(".category_2").change(function(){
        
            var id = $(this).val();
            $("#related_to_images_2").empty();
            $(".loader_2").fadeIn(300);
            
            $.ajax({
                url: "<?php echo base_url() . "related/getPosts/"; ?>" + id,
                type: "GET",
                dataType: "json",
                success: function(data){
                    
                    $(".loader_2").fadeOut(300);
                    
                    for(var i = 0; i < data.length; i++){
                        
                        if (data[i].Shoes !== undefined){
                            $("#related_to_images_2").append("<div class='imageWrap esq'>" +
                                "<div class='rel_wrap'>" +
                                "<img id='" + data[i].Shoes.objectId + "_2_' class='ok_related' src='<?php echo relative_url() . $images . "button_ok"; ?>' />" +
                                "<img id='" + data[i].Shoes.objectId + "_2' class='related_image' src='" + data[i].Shoes.image.url + "' width='90' height='90' title='" + data[i].Shoes.category.description + " | " + data[i].Shoes.model + "/" + data[i].Shoes.brand + "' />" +
                                "<input type='hidden' class='brand' value='" + data[i].Shoes.category.description + " | " + data[i].Shoes.model + "/" + data[i].Shoes.brand + "' />" +
                                "</div>" +
                                "</div>");
                        }
                    }
                }
            });
        });
    });
</script>



<div class="related_holder">

    <div class="related_to_holder roundBordersP_4px shadow esq">
        <span class="title">Main Shoes</span>
        <div class="clear"></div>
        <div class="related_to_search roundBordersP_2px esq">
            <input type="text" id="search_ori" class="search" value="Search" />
        </div>

        <div class="clear"></div>

        <div>
            <span style="font-style: italic">Categories:</span> <select class="category_1">
                <option>.::Select::.</option>
                <?php foreach ($categories as $category) { ?>
                    <option value="<?php echo $category->objectId; ?>"><?php echo $category->description; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="clear"></div>

        <div class="related_to_images" id="holder_1">

            <div id="related_to_images_1"></div>
            <img class="loader_1 loader" src="<?php echo base_url() . $images . "shoes_loader.gif"; ?>" />
        </div>
    </div>

    <div class="related_to_holder roundBordersP_4px shadow dir">

        <span class="title">Related Shoes</span>
        <div class="clear"></div>
        <div class="related_to_search roundBordersP_2px esq">
            <input type="text" id="search_rel" class="search" value="Search" />
        </div>

        <div class="clear"></div>

        <div>
            <span style="font-style: italic">Categories:</span> <select class="category_2">
                <option>.::Select::.</option>
                <?php foreach ($categories as $category) { ?>
                    <option value="<?php echo $category->objectId; ?>"><?php echo $category->description; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="clear"></div>

        <input type="checkbox" id="checkAll" /><span>Check all</span>

        <div class="related_to_images" id="holder_2">
            <div id="related_to_images_2"></div>
            <img class="loader_2 loader" src="<?php echo base_url() . $images . "shoes_loader.gif"; ?>" />
        </div>
    </div>

    <?php echo form_open(base_url() . "related/save", array("method" => "post", "id" => "form", "class" => "esq")); ?>

    <input type="hidden" id="ori_image" name="ori_image" />

    <div class="ids_holder">

    </div>

    <div class="clear"></div>

    <div class="dir" style="width: 960px; margin-top: 20px">
        <input type="submit" class="submit_bt dir" value="" title="Save"/>
        <a class="bt" href="<?php echo base_url(); ?>" >Home</a>
    </div>

    <?php echo form_close(); ?>
</div>

