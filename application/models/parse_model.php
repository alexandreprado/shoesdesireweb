<?php

class Parse_Model extends CI_Model {

    private $headers = array("X-Parse-Application-Id: QsGVQvURfVQn7PIxfJIMSIMgKaIxlZXmJ7EbStYZ", "X-Parse-REST-API-Key: foKJyiCmAuRTCenNnz5VgM4xrzbPphAh2R55wuzZ", "Content-Type: application/json");
    private $headers_image = array("X-Parse-Application-Id: QsGVQvURfVQn7PIxfJIMSIMgKaIxlZXmJ7EbStYZ", "X-Parse-REST-API-Key: foKJyiCmAuRTCenNnz5VgM4xrzbPphAh2R55wuzZ", "Content-Type: image/png");

    function get($class, $where, $options) {

        if (is_array($where)) {
            $where = "where=" . urlencode(json_encode($where));
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/classes/$class/?$where&$options"); // /objectId para retornar um objeto especifico
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
//        $info = curl_getinfo($ch);
        curl_close($ch);

        return json_decode($output);
    }

    function post($class, $array) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/classes/$class/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array));
        $response = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $retorno = new stdClass();
        $retorno->status = $http_status;
        $retorno->data = json_decode($response);

        return $retorno;
    }

    function upload($file, $file_name) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/files/$file_name");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers_image);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $file);
        $response = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $retorno = new stdClass();
        $retorno->status = $http_status;
        $retorno->data = json_decode($response);

        return $retorno;
    }

    function put($class, $values, $options) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/classes/$class/?$options");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($values));
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }

    function delete($class, $where, $options) {

        if (is_array($where)) {
            $where = "where=" . urlencode(json_encode($where));
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/classes/$class/?$where&$options");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }

    function batch($data) {

        $valores = array();

        for ($i = 0; $i < count($data); $i++) {

            $valores[$i] = array('method' => 'POST', 'path' => '/1/classes/' . $data[$i]["classe"], 'body' => $data[$i]["dados"]);
        }

        $data = array('requests' => $valores);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/batch");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $output = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($output);


        return $result;
    }

    function batch_update($data) {

        $valores = array();

        for ($i = 0; $i < count($data); $i++) {

            $valores[$i] = array('method' => 'PUT', 'path' => '/1/classes/' . $data[$i]["classe"] . "/" . $data[$i]["objectId"], 'body' => $data[$i]["dados"]);
        }

        $data = array('requests' => $valores);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/batch");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $output = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($output);


        return $result;
    }

    function batch_delete($data) {

        for ($i = 0; $i < count($data); $i++) {

            $valores[$i] = array('method' => 'DELETE', 'path' => '/1/classes/' . $data[$i]["classe"] . '/' . $data[$i]["objectId"]);
        }

        $data = array('requests' => $valores);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/batch");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $output = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($output);


        return $result;
    }

    function cloud_call($function, $array) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.parse.com/1/functions/$function");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array));
        $retorno = curl_exec($ch);
        curl_close($ch);

        return json_decode($retorno);
    }

}

?>