<?php

class IQEngine_Model extends CI_Model {

    private $api_key = "0826a56f7d19428b93766fb0974059f4";
    private $secret = "490367e5f9f64ae7b44906aa5b6702ed";
    private $header = array("Content-Type: multipart/form-data");

    function upload($fields) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.iqengines.com/v1.2/object/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $response = curl_exec($ch);

        return json_decode($response);
    }
    
    function append($obj_id, $fields) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.iqengines.com/v1.2/object/$obj_id/images/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $response = curl_exec($ch);

        return json_decode($response);
    }
    
    function getApiKey() {
        return $this->api_key;
    }

    function getSecretKey() {
        return $this->secret;
    }

}

?>