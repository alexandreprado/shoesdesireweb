<?php

class Foursquare_Model extends CI_Model {

    private $client_id = "1QYXVZCOP04OTIFOIW3V1IP1RJWRRSZRLLN4XPWQKVGA3KPF";
    private $cliente_scret = "355QUURWDHUXQDK0AO42JYX4PNXL0XKR4VILDDGW5TPLFKDR";

    function get($venue, $city, $state) {
        
        $string = "https://api.foursquare.com/v2/venues/search?query=$venue&near=$city,$state&client_id=$this->client_id&client_secret=$this->cliente_scret&v=" . date('Ymd');
        
//        echo $string;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }
    
    function getVenue($venue_id) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.foursquare.com/v2/venues/$venue_id?client_id=$this->client_id&client_secret=$this->cliente_scret&v=" . date('Ymd'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }

}

?>