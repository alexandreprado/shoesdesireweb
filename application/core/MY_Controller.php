<?php

class MY_Controller extends CI_Controller {

    public $img_dir = "";
    public $inc_dir = "";
    public $dados = array();

    public function __construct() {
        parent::__construct();

        $this->img_dir = $this->config->item('images');
        $this->inc_dir = $this->config->item('includes');

        // Ao inicializar o controlador, já verifica se o usuário está logado
        // Apagar isso quando implementar o login
        $this->dados['images'] = $this->img_dir;
//        $this->dados['user'] = $user;
//        $this->dados['segmentos'] = $segmentos;
        $this->dados['includes'] = $this->inc_dir;
        $this->dados['error'] = "";

//        $this->usuario_logado(); // Chama o método abaixo
    }

    // Método protegido, usado apenas na classe, para verificar se o usuário está logado

    protected function usuario_logado() {
        // Carrega o Helper URL para usar o método "redirect()"

        $this->load->helper('url');

        // Usuário não está logado? Então vai para a página de login

        if (!$this->session->userdata('user_logado')) {
            redirect('/login', 'refresh');
        } else {
            $this->load->model("Model_header");

            $user_id = $this->session->userdata('user_id');

            $user = $this->Model_header->buscaUsuarioById($user_id);

            $segmentos = $this->uri->segment_array();

            $this->dados['images'] = $this->img_dir;
            $this->dados['user'] = $user;
            $this->dados['segmentos'] = $segmentos;
            $this->dados['includes'] = $this->inc_dir;
            $this->dados['error'] = "";
        }
    }

    public function do_upload($file, $path) {

        $this->load->library('Jupload');

        $this->jupload->do_upload($file);

        if ($this->jupload->uploaded) {

            $uploaddir = complete_url() . $path;

            $this->jupload->file_max_size = 614400; // 600kb
//            $e = new Jupload();
            $this->jupload->image_max_width = 2500;
            $this->jupload->image_max_height = 2500;
            $this->jupload->image_min_width = 320;
            $this->jupload->image_min_height = 320;
            $this->jupload->file_new_name_body = 'photo_temp_' . time();
            $this->jupload->image_convert = 'jpg';
            $this->jupload->image_resize = true;
            $this->jupload->image_x = 640;
            $this->jupload->image_y = 640;
            $this->jupload->image_ratio = true;
            $this->jupload->file_overwrite = true;
            $this->jupload->file_auto_rename = false;
            $this->jupload->jpeg_quality = 100;

            $this->jupload->Process($uploaddir);

            if ($this->jupload->processed) {
                echo json_encode(array("result" => true, "image_x" => $this->jupload->image_dst_x, "image_y" => $this->jupload->image_dst_y, "image" => $this->jupload->file_dst_name));
            } else {
                echo json_encode(array("result" => false, "error" => $this->jupload->error));
            }
        } else {
            echo json_encode(array("result" => false, "error" => $this->jupload->error));
        }
    }

    public function do_crop($file, $path, $w, $h, $x, $y) {

        $this->load->library('Jupload');

        $this->jupload->do_upload(complete_url() . $file, 'pt_BR');

        if ($this->jupload->uploaded) {
            $this->jupload->image_convert = 'png';
            $this->jupload->file_src_name_body = "cropped_" . time(); // hard name
            $this->jupload->file_overwrite = true;
            $this->jupload->file_auto_rename = false;
            $this->jupload->image_resize = true;
            $this->jupload->image_x = 320; //size of final picture
            $this->jupload->image_y = 320; //size of final picture

            $this->jupload->jcrop = true;
            $this->jupload->rect_w = $w;
            $this->jupload->rect_h = $h;
            $this->jupload->posX = $x;
            $this->jupload->posY = $y;
            $this->jupload->jpeg_quality = 100;
            $this->jupload->Process(complete_url() . $path);

//            $this->jupload->clean();
//            @unlink(complete_url() . $file);

            if ($this->jupload->processed) {
                echo json_encode(array("result" => true, "image" => $this->jupload->file_dst_name));
            } else {
                echo json_encode(array("result" => false, "error" => $this->jupload->error));
            }
        } else {
            echo json_encode(array("result" => false, "error" => $this->jupload->error));
        }
    }

}