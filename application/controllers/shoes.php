<?php

class Shoes extends MY_Controller {

    private $uploads = "";

    public function __construct() {
        parent::__construct();

        $this->load->model('parse_model');
        $this->load->model('foursquare_model');
        $this->load->model('iqengine_model');
        $this->uploads = $this->config->item('uploads');
        $this->dados['uploads'] = $this->uploads;
    }

    public function index($userId = "", $page = "") {

        $per_page = 100;
        $total_pages = 0;
        $user = "";
        $posts = array();

        if ($userId) {
            $user_ar = $this->parse_model->get("_User/$userId", "", "");
            $user = property_exists($user_ar, "objectId") ? $user_ar->objectId : "";
        }

        if ($page && $user) {

            $rr = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $user));

            $total = $this->parse_model->get("Post", $rr, "count=1;limit=0");

            $total_itens = $total->count;

            $total_pages = intval($total_itens / $per_page);

            $begin = ($page * $per_page) - $per_page;

            $ar = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $user));

            $posts_ar = $this->parse_model->get("Post", $ar, "limit=$per_page;skip=$begin;include=Shoes;order=-createdAt");
            
//            print_r($posts_ar);
//            
//            exit();

            $posts = $posts_ar->results;
        }

        $users_p = $this->parse_model->get("_User", "order=username", "");

        $this->dados['users'] = $users_p->results;
        $this->dados['userId'] = $user;
        $this->dados['total_pages'] = $total_pages;
        $this->dados['page'] = $page;
        $this->dados['posts'] = $posts;

        $this->load->main_view("shoes/shoes", $this->dados);
    }

    public function shoes($userId = "", $page = "") {

        $per_page = 100;
        $total_pages = 0;
        $user = "";
        $posts = array();

        if ($userId) {
            $user_ar = $this->parse_model->get("_User/$userId", "", "");
//             print_r($user_ar);
//            
//            exit();
            $user = property_exists($user_ar, "objectId") ? $user_ar->objectId : "";
        }

        if ($page && $user) {

            $rr = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $user));

            $total = $this->parse_model->get("Post", $rr, "count=1;limit=0");

            $total_itens = $total->count;

            $total_pages = intval($total_itens / $per_page);

            $begin = ($page * $per_page) - $per_page;

            $ar = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $user));

            $posts_ar = $this->parse_model->get("Post", $ar, "limit=$per_page;skip=$begin;include=Shoes;order=-createdAt");
            
//            print_r($posts_ar);
//            
//            exit();

            $posts = $posts_ar->results;
        }

        $users_p = $this->parse_model->get("_User", "order=username", "");

        $this->dados['users'] = $users_p->results;
        $this->dados['userId'] = $user;
        $this->dados['total_pages'] = $total_pages;
        $this->dados['page'] = $page;
        $this->dados['posts'] = $posts;

        $this->load->main_view("shoes/shoes", $this->dados);
    }

    public function novo() {

        $this->load->library('form_validation');

        $users = $this->parse_model->get("_User", "", "order=username");
        $categories = $this->parse_model->get("ShoesCategory", "", "order=description");

        $ar = array();
        $ar[""] = ".::Select::.";

        foreach ($users->results as $user) {
            $ar[$user->objectId] = $user->username;
        }

        $br = array();
        $br[""] = ".::Select::.";

        foreach ($categories->results as $category) {
            $br[$category->objectId] = $category->description;
        }

        $this->dados['users'] = $ar;
        $this->dados['categories'] = $br;

        $this->form_validation->set_error_delimiters('<div class="form_error"><span>', '</span></div>');
        $this->form_validation->set_rules('user_id', 'User', 'required|trim|xss_clean');
        $this->form_validation->set_rules('category_id', 'Category', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('image_name', 'Image', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('image_local', 'Image', 'required|trim|xss_clean');
        $this->form_validation->set_rules('upload_1_parse', 'Image', 'required|trim|xss_clean');
        $this->form_validation->set_rules('brand', 'Brand', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('model', 'Model', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('price', 'Price', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('rating', 'Rating', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('venue_id', 'Venue', 'required|trim|xss_clean');

        $this->form_validation->set_message('required', '%s can not be empty.');

        if ($this->form_validation->run() == FALSE) {
            $this->load->main_view("shoes/shoes_adicionar", $this->dados);
        } else {

            $post = $this->input->post();

            $image_1_parse = complete_url() . $this->uploads . $post['upload_1_parse'];
            $content_1_parse = file_get_contents($image_1_parse);

            $image_1_iq_name = $post['upload_1_iq'];
            $image_1_iq = complete_url() . $this->uploads . $post['upload_1_iq'];

            $image_2_iq_name = $post['upload_2_iq'];
            $image_2_iq = complete_url() . $this->uploads . $post['upload_2_iq'];

            $image_3_iq_name = $post['upload_3_iq'];
            $image_3_iq = complete_url() . $this->uploads . $post['upload_3_iq'];

            $image_4_iq_name = $post['upload_4_iq'];
            $image_4_iq = complete_url() . $this->uploads . $post['upload_4_iq'];

            $image_parse = $this->parse_model->upload($content_1_parse, "image.png");

            if ($image_parse->status === 201) { // Se o upload da imagem ocorreu tudo certo, prossegue
                $price = $post['price'] ? intval($post['price']) : NULL;
                $rating = $post['rating'] ? intval($post['rating']) : NULL;

                $shoes = array("brand" => $post['brand'], "model" => $post['model'], "price" => $price,
                    "rating" => $rating, "category" => array("__type" => "Pointer", "className" => "ShoesCategory", "objectId" => $post['category_id']),
                    "image" => array("name" => $image_parse->data->name, "url" => $image_parse->data->url, "__type" => 'File'));
//
                $result1 = $this->parse_model->post("Shoes", $shoes); // Insere o shoes no parse

                if ($result1->status === 201) {

                    $shoesId = $result1->data->objectId;

                    if (key_exists('venue_id', $post) && $post['venue_id']) {

                        $counter = 0;

                        foreach ($post['venue_id'] as $venue) {

                            if ($venue) {

                                $store = array("venue_id" => $venue, "website" => $post['site'][$counter],
                                    "shoes" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $shoesId));

                                $this->parse_model->post("Store", $store); // Insere o store caso venha preenchido
                            }
                            $counter++;
                        }
                    }

                    $post_parse = array("comment" => $post['comment'],
                        "Shoes" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $shoesId),
                        "user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $post['user_id']));

                    $result3 = $this->parse_model->post("Post", $post_parse); // insere o post no parse

                    if ($result3->status === 201) {

                        $meta = array("brand" => $post['brand'], "model" => $post['model'], "category" => $post['category_id'], "parse_id" => $shoesId);

                        $api_key = $this->iqengine_model->getApiKey();

                        $time_stamp = date('YmdHis', strtotime("+4 hours", strtotime(date('YmdHis'))));

                        $iq_engine = array("api_key" => $api_key, "time_stamp" => $time_stamp,
                            "json" => "1", "name" => $post['model'], "images" => $image_1_iq_name, "meta" => json_encode($meta));

                        $api_sig = $this->generateIQSignature($iq_engine);

                        $iq_array = array('api_key' => $api_key, 'api_sig' => $api_sig, 'time_stamp' => $time_stamp,
                            'name' => $post['model'], 'meta' => json_encode($meta), 'json' => "1", 'images' => "@" . $image_1_iq . ";type=image/jpg");

//                        $return_iq_1 = $this->iqengine_model->upload($iq_array); // insere a imagem no iq_engine
//
//                        if ($return_iq_1->error === 0) {
//
//                            $obj_id = $return_iq_1->obj_id;
//
//                            if ($image_2_iq_name) {
//
//                                $iq_engine = array("api_key" => $api_key, "time_stamp" => $time_stamp,
//                                    "json" => "1", "images" => $image_2_iq_name);
//
//                                $api_sig = $this->generateIQSignature($iq_engine);
//
//                                $iq_array = array('api_key' => $api_key, 'api_sig' => $api_sig, 'time_stamp' => $time_stamp,
//                                    'json' => "1", 'images' => "@" . $image_2_iq . ";type=image/jpg");
//
//                                $return_iq_2 = $this->iqengine_model->append($obj_id, $iq_array);
//
//                                if ($return_iq_2->error !== 0) {
//                                    $this->session->set_flashdata("error", $return_iq_2->comment);
//                                    redirect('shoes/novo', 'refresh');
//                                }
//                            }
//
//                            if ($image_3_iq_name) {
//
//                                $iq_engine = array("api_key" => $api_key, "time_stamp" => $time_stamp,
//                                    "json" => "1", "images" => $image_3_iq_name);
//
//                                $api_sig = $this->generateIQSignature($iq_engine);
//
//                                $iq_array = array('api_key' => $api_key, 'api_sig' => $api_sig, 'time_stamp' => $time_stamp,
//                                    'json' => "1", 'images' => "@" . $image_3_iq . ";type=image/jpg");
//
//                                $return_iq_3 = $this->iqengine_model->append($obj_id, $iq_array);
//
//                                if ($return_iq_3->error !== 0) {
//                                    $this->session->set_flashdata("error", $return_iq_3->comment);
//                                    redirect('shoes/novo', 'refresh');
//                                }
//                            }
//
//                            if ($image_4_iq_name) {
//
//                                $iq_engine = array("api_key" => $api_key, "time_stamp" => $time_stamp,
//                                    "json" => "1", "images" => $image_4_iq_name);
//
//                                $api_sig = $this->generateIQSignature($iq_engine);
//
//                                $iq_array = array('api_key' => $api_key, 'api_sig' => $api_sig, 'time_stamp' => $time_stamp,
//                                    'json' => "1", 'images' => "@" . $image_4_iq . ";type=image/jpg");
//
//                                $return_iq_4 = $this->iqengine_model->append($obj_id, $iq_array);
//
//                                if ($return_iq_4->error !== 0) {
//                                    $this->session->set_flashdata("error", $return_iq_4->comment);
//                                    redirect('shoes/novo', 'refresh');
//                                }
//                            }
                        $this->session->set_flashdata("success", "Item saved successfully!");
                        redirect('shoes/novo', 'refresh');
//                        } else {
//                            $this->session->set_flashdata("error", $return_iq_1->comment);
//                            redirect('shoes/novo', 'refresh');
//                        }
                    }
                }
            }
            $this->session->set_flashdata("error", "An error occurred. Please try again in a few minutes.");
            redirect('shoes/novo', 'refresh');
        }
    }

    public function editar($id = "") {

        $this->load->library('form_validation');

        if ($id) {

            $post_entity = $this->parse_model->get("Post/$id", "include=Shoes,Shoes.category,user", "");
            $shoes_entity = $post_entity->Shoes;

            $st = array("shoes" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $post_entity->Shoes->objectId));

            $stores = $this->parse_model->get("Store", $st, "");

            $users = $this->parse_model->get("_User", "", "order=username");
            $categories = $this->parse_model->get("ShoesCategory", "", "order=description");

            $ar = array();
            $ar[""] = ".::Select::.";

            foreach ($users->results as $user) {
                $ar[$user->objectId] = $user->username;
            }

            $br = array();
            $br[""] = ".::Select::.";

            foreach ($categories->results as $category) {
                $br[$category->objectId] = $category->description;
            }

            $array_store = array();

            foreach ($stores->results as $store) {

                $venue = $this->lookForVenue($store->venue_id);

                $array_store[] = array('venue_id' => $store->venue_id, 'name' => $venue->response->venue->name,
                    'city' => (property_exists($venue->response->venue->location, "city") ? $venue->response->venue->location->city : ""),
                    'state' => (property_exists($venue->response->venue->location, "state") ? $venue->response->venue->location->state : ""),
                    'website' => (property_exists($store, 'website') ? $store->website : ""), "address" => (property_exists($venue->response->venue->location, "address") ? $venue->response->venue->location->address : ""));
            }

            $this->form_validation->set_error_delimiters('<div class="form_error"><span>', '</span></div>');
            $this->form_validation->set_rules('user_id', 'User', 'required|trim|xss_clean');
            $this->form_validation->set_rules('category_id', 'Category', 'required|trim|xss_clean');
            $this->form_validation->set_rules('brand', 'Brand', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('model', 'Model', 'required|trim|xss_clean');

            $this->dados['stores'] = $array_store;
            $this->dados['users'] = $ar;
            $this->dados['categories'] = $br;
            $this->dados['post'] = $post_entity;

            $this->form_validation->set_message('required', '%s can not be empty.');

            if ($this->form_validation->run() == FALSE) {

                if ($post_entity) {
                    $this->load->main_view("shoes/shoes_editar", $this->dados);
                } else {
                    redirect('shoes', 'refresh');
                }
            } else {

                $post = $this->input->post();

                $image_1_parse = complete_url() . $this->uploads . $post['upload_1_parse'];
                $content_1_parse = file_get_contents($image_1_parse);

                $image_1_iq_name = $post['upload_1_iq'];
                $image_1_iq = complete_url() . $this->uploads . $post['upload_1_iq'];

//                $image_2_iq_name = $post['upload_2_iq'];
//                $image_2_iq = complete_url() . $this->uploads . $post['upload_2_iq'];
//
//                $image_3_iq_name = $post['upload_3_iq'];
//                $image_3_iq = complete_url() . $this->uploads . $post['upload_3_iq'];
//
//                $image_4_iq_name = $post['upload_4_iq'];
//                $image_4_iq = complete_url() . $this->uploads . $post['upload_4_iq'];

                $image_parse->status = 201;

                if ($post['atualiza_imagem'] === '1') {
                    $image_parse = $this->parse_model->upload($content_1_parse, "image.png");
                }

                if ($image_parse->status === 201) { // Se o upload da imagem ocorreu tudo certo, prossegue
                    $price = $post['price'] ? intval($post['price']) : NULL;
                    $rating = $post['rating'] ? intval($post['rating']) : NULL;

                    $shoes = array("brand" => $post['brand'], "model" => $post['model'], "price" => $price,
                        "rating" => $rating, "category" => array("__type" => "Pointer", "className" => "ShoesCategory", "objectId" => $post['category_id']));

                    if ($post['atualiza_imagem'] === '1') {
                        $shoes['image'] = array("name" => $image_parse->data->name, "url" => $image_parse->data->url, "__type" => 'File');
                    }
//
                    $result1 = $this->parse_model->put("Shoes/$shoes_entity->objectId", $shoes, ""); // Atualiza o shoes no parse
//                    print_r($result1);
//                    exit();

                    if ($result1->updatedAt) {

//                        $shoesId = $result1->data->objectId;

                        if (key_exists('venue_id', $post) && $post['venue_id']) {

                            $counter = 0;

                            foreach ($post['venue_id'] as $venue) {

                                if ($venue) {

                                    $store = array("venue_id" => $venue, "website" => $post['site'][$counter],
                                        "shoes" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $shoes_entity->objectId));

                                    $this->parse_model->post("Store", $store); // Insere o store caso venha preenchido
                                }
                                $counter++;
                            }
                        }

                        if (key_exists('venues_removed', $post) && $post['venues_removed']) {

                            $counter = 0;

                            foreach ($post['venues_removed'] as $venue) {

                                if ($venue) {

                                    $st = array("venue_id" => $venue, "shoes" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $shoes_entity->objectId));

                                    $store = $this->parse_model->get("Store", $st, "limit=1");

                                    if ($store) {
                                        $this->parse_model->delete("Store/" . $store->results[0]->objectId, "", ""); // Deleta o store caso venha preenchido
                                    }
                                }
                                $counter++;
                            }
                        }

                        $post_parse = array("comment" => $post['comment'],
                            "Shoes" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $shoes_entity->objectId),
                            "user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $post['user_id']));

                        $result3 = $this->parse_model->put("Post/$post_entity->objectId", $post_parse, ""); // atualiza o post no parse

                        if ($result3->updatedAt) {

//                            $meta = array("brand" => $post['brand'], "model" => $post['model'], "category" => $post['category_id'], "parse_id" => $shoes_entity->objectId);
//
//                            $api_key = $this->iqengine_model->getApiKey();
//
//                            $time_stamp = date('YmdHis', strtotime("+4 hours", strtotime(date('YmdHis'))));
//
//                            $iq_engine = array("api_key" => $api_key, "time_stamp" => $time_stamp,
//                                "json" => "1", "name" => $post['model'], "images" => $image_1_iq_name, "meta" => json_encode($meta));
//
//                            $api_sig = $this->generateIQSignature($iq_engine);
//
//                            $iq_array = array('api_key' => $api_key, 'api_sig' => $api_sig, 'time_stamp' => $time_stamp,
//                                'name' => $post['model'], 'meta' => json_encode($meta), 'json' => "1", 'images' => "@" . $image_1_iq . ";type=image/jpg");

                            $this->session->set_flashdata("success", "Item saved successfully!");
                            redirect('shoes/editar/' . $id, 'refresh');
                        }
                    }
                }
                $this->session->set_flashdata("error", "An error occurred. Please try again in a few minutes.");
                redirect('shoes', 'refresh');
            }
        } else {
            redirect('shoes', 'refresh');
        }
    }

    public function delete($id = "") {

        if ($id) {

            $this->parse_model->delete("Post/$id", "", "");
            $this->session->set_flashdata("success", "Item deleted successfully!");
            redirect('shoes', 'refresh');
        } else {
            redirect('shoes', 'refresh');
        }
    }

    public function upload() {

        $file = $_FILES['uploadfile'];

        $this->do_upload($file, $this->uploads);
    }

    public function crop() {

        $file = $this->input->post('tempfile');
        $w = $this->input->post('image_w');
        $h = $this->input->post('image_h');
        $x = $this->input->post('image_x');
        $y = $this->input->post('image_y');

        $this->do_crop($file, $this->uploads, $w, $h, $x, $y);
    }

    public function sendKooaba() {

        // Se nao funfar no servidor, rodar esse script
//        $w = stream_get_wrappers();
//        echo 'openssl: ', extension_loaded('openssl') ? 'yes' : 'no', "\n";
//        echo 'http wrapper: ', in_array('http', $w) ? 'yes' : 'no', "\n";
//        echo 'https wrapper: ', in_array('https', $w) ? 'yes' : 'no', "\n";
//        echo 'wrappers: ', var_dump($w);
        // Se algum for false, habilitar as seguintes conf no php.ini
        //extension=php_openssl.dll
        //allow_url_include = On
        //allow_url_fopen = On

        $this->load->library('Kooaba');

        $file_path = $this->input->post('file_path');
        $file_name = $this->input->post('file_name');
        $title = $this->input->post('title');
        $metadata = NULL;

        $result = $this->kooaba->do_upload($file_path, $file_name, $title, $metadata);

        echo $result;
    }

    public function lookForVenues($venue, $city, $state) {

        $response = $this->foursquare_model->get($venue, $city, $state);

        if ($response->meta->code === 200) {

            $options = "<option value=''>.::Select::.</option>";

            foreach ($response->response->venues as $venue) {
                $options .= "<option value='" . $venue->id . "'>" . $venue->name . " | " . $venue->location->address . "</option>";
            }
        } else {
            $options = "<option>No venues found</option>";
        }

        echo $options;
    }

    public function lookForVenuesEdit($venue, $city, $state) {

//        echo $venue . ", " . $city . ", " . $state . "<br />";

        $response = $this->foursquare_model->get($venue, $city, $state);
//        print_r($response);
//        exit();
        $options = array();

        if ($response->meta->code === 200) {

            $options[''] = ".::Select::.";

            foreach ($response->response->venues as $venue) {
                $options[$venue->id] = $venue->name . " | " . $venue->location->address;
            }
        } else {
            $options = array();
        }

        return $options;
    }

    public function lookForVenue($venue_id) {

        $response = $this->foursquare_model->getVenue($venue_id);

//        print_r($response) . "<br />";
//        print_r($response->response->venue->name)."<br />";
//        print_r($response->response->venue->location->city)."<br />";
//        print_r($response->response->venue->location->state)."<br />";
//        
//        return false;

        if ($response->meta->code !== 200) {

            return array();
        }

        return $response;
    }

    public function generateIQSignature($array) {

        ksort($array);

        $return = "";

        foreach ($array as $key => $value) {
            $return .= $key . $value;
        }

        $secret = $this->iqengine_model->getSecretKey();

        $my_sign = hash_hmac("sha1", $return, $secret);

        return $my_sign;
    }

    public function getPosts($user_id, $page = 1) {

        $per_page = 100;

        $rr = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $user_id));

        $total = $this->parse_model->get("Post", $rr, "count=1;limit=0");

        $total_itens = $total->count;

        $total_pages = intval($total_itens / $per_page);

        $begin = ($page * $per_page) - $per_page;

        $ar = array("user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $user_id));

        $posts = $this->parse_model->get("Post", $ar, "limit=$per_page;skip=$begin;include=Shoes;order=-createdAt");

        $result = array();

        foreach ($posts->results as $post) {
            $result['posts'][] = $post;
        }

        $result['total_pages'] = $total_pages;
        $result['page'] = $page;

        echo json_encode($result);
    }

}

?>
