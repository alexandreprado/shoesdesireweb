<?php

class Related extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('parse_model');
    }

    public function index() {

//        $shoes = $this->parse_model->get('Post', '', 'order=createdAt;include=Shoes,Shoes.category;limit=1000');
        
        $categories = $this->parse_model->get("ShoesCategory", "", "order=description");

//        $this->dados['shoes'] = $shoes->results;
        $this->dados['categories'] = $categories->results;

        $this->load->main_view('related/related', $this->dados);
    }

    public function save() {

        $post = $this->input->post();

        $main_image = $post['ori_image'];
        $erro = false;

        if (key_exists('rel_images', $post) && $main_image) {

            foreach ($post['rel_images'] as $shoe) {

                $arr = array("shoe" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $main_image),
                    "relatedto" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $shoe));

                $response = $this->parse_model->post("Related", $arr);

                if ($response->status !== 201) {
                    $erro = true;
                }
            }

            $update = array("hasRelated" => true);

            $this->parse_model->put("Shoes/$main_image", $update, "");
        }

        if ($erro) {
            $this->session->set_flashdata("error", "An error occurred. Please try again in a few minutes.");
            redirect('related/related', 'refresh');
        } else {
            $this->session->set_flashdata("success", "Item saved successfully!");
            redirect('related/related', 'refresh');
        }
    }

    public function getPosts($categoryId) {

        $ar = array("Shoes" => array('$inQuery' => array("where" => array("category" => array("__type" => "Pointer", "className" => "ShoesCategory", "objectId" => $categoryId)), "className" => "Shoes")));

        $posts = $this->parse_model->get("Post", $ar, "include=Shoes,Shoes.category");

        echo json_encode($posts->results);
    }

}

?>