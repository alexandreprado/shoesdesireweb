<?php

class Login extends CI_Controller {
    
    public $img_dir = "";

    public function __construct() {
        parent::__construct();
        
        $this->img_dir = base_url() . $this->config->item('images');

        $this->load->library('form_validation');;

        if ($this->session->userdata('user_logado')) {
            redirect('/', 'refresh');
        }
    }

    public function index() {

        $dados['images'] = $this->img_dir;

        $this->form_validation->set_error_delimiters('<div id="login_erro"><span>', '</span></div>');
        $this->form_validation->set_rules('username', 'username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'required|trim|md5|callback_r_valida_login');
        $this->form_validation->set_message('required', 'Todos os campos são obrigatórios.');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header_login');
            $this->load->view('login', $dados);
            $this->load->view('templates/footer');
        } else {
            redirect('/', 'refresh');
        }
    }

    public function r_valida_login($password) {

        $this->load->model('Model_login');

        $username = $this->input->post('username', TRUE);

        $resultado = $this->Model_login->verifica_login($username, $password);

        if ($resultado == FALSE) {
            $this->form_validation->set_message('r_valida_login', 'Não encontramos seu cadastro no sistema.');
            
            return FALSE;
        } else {
            $dados_login = array(
                'user_logado' => TRUE,
                'user_id' => $resultado
            );

            $this->session->set_userdata($dados_login);

            return TRUE;
        }
    }

}

?>
