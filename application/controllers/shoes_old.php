<?php

class Shoes extends MY_Controller {

    private $uploads = "";

    public function __construct() {
        parent::__construct();

        $this->load->model('parse_model');
        $this->load->model('foursquare_model');
        $this->uploads = $this->config->item('uploads');
        $this->dados['uploads'] = $this->uploads;
    }

    public function index() {
        redirect('shoes/novo');
    }

    public function novo() {

        $this->load->library('form_validation');

        $users = $this->parse_model->get("_User", "", "order=username");
        $categories = $this->parse_model->get("ShoesCategory", "", "order=description");

        $ar = array();
        $ar[""] = ".::Select::.";

        foreach ($users->results as $user) {
            $ar[$user->objectId] = $user->username;
        }

        $br = array();
        $br[""] = ".::Select::.";

        foreach ($categories->results as $category) {
            $br[$category->objectId] = $category->description;
        }

        $this->dados['users'] = $ar;
        $this->dados['categories'] = $br;

        $this->form_validation->set_error_delimiters('<div class="form_error"><span>', '</span></div>');
        $this->form_validation->set_rules('user_id', 'User', 'required|trim|xss_clean');
        $this->form_validation->set_rules('category_id', 'Category', 'required|trim|xss_clean');
        $this->form_validation->set_rules('image_name', 'Image', 'required|trim|xss_clean');
        $this->form_validation->set_rules('image_local', 'Image', 'required|trim|xss_clean');
        $this->form_validation->set_rules('brand', 'Brand', 'required|trim|xss_clean');
        $this->form_validation->set_rules('model', 'Model', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('price', 'Price', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('rating', 'Rating', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('venue_id', 'Venue', 'required|trim|xss_clean');

        $this->form_validation->set_message('required', '%s can not be empty.');

        if ($this->form_validation->run() == FALSE) {
            $this->load->main_view("shoes/shoes_adicionar", $this->dados);
        } else {

            $post = $this->input->post();

//            $price = doubleval(desconverterMoeda($post['price']));

            $price = $post['price'] ? intval($post['price']) : NULL;
            $rating = $post['rating'] ? intval($post['rating']) : NULL;

            $shoes = array("brand" => $post['brand'], "model" => $post['model'], "price" => $price,
                "rating" => $rating, "kooaba_uuid" => $post['kooaba_uuid'],
                "category" => array("__type" => "Pointer", "className" => "ShoesCategory", "objectId" => $post['category_id']),
                "image" => array("name" => $post['image_name'], "url" => $post['image_url'], "__type" => 'File'));

            $result1 = $this->parse_model->post("Shoes", $shoes);

            if ($result1->status === 201) {

                $shoesId = $result1->data->objectId;

                if (key_exists('venue_id', $post) && $post['venue_id']) {

                    $store = array("venue_id" => $post['venue_id'],
                        "shoes" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $shoesId));

                    $result2 = $this->parse_model->post("Store", $store);
                } else {
                    $result2 = new stdClass();
                    $result2->status = 201;
                }

                if ($result2->status === 201) {

                    $post = array("comment" => $post['comment'],
                        "Shoes" => array("__type" => "Pointer", "className" => "Shoes", "objectId" => $shoesId),
                        "user" => array("__type" => "Pointer", "className" => "_User", "objectId" => $post['user_id']));

                    $result3 = $this->parse_model->post("Post", $post);

                    if ($result3->status === 201) {
                        $this->session->set_flashdata("success", "Item saved successfully!");
                        redirect('shoes/novo', 'refresh');
                    }
                }
            }
            $this->session->set_flashdata("error", "An error occurred. Please try again in a few minutes.");
            redirect('shoes/novo', 'refresh');
        }
    }

    public function upload() {

        $file = $_FILES['uploadfile'];

        $this->do_upload($file, $this->uploads);
    }

    public function crop() {

        $file = $this->input->post('tempfile');
        $w = $this->input->post('image_w');
        $h = $this->input->post('image_h');
        $x = $this->input->post('image_x');
        $y = $this->input->post('image_y');

        $this->do_crop($file, $this->uploads, $w, $h, $x, $y);
    }

    public function sendKooaba() {

        // Se nao funfar no servidor, rodar esse script
//        $w = stream_get_wrappers();
//        echo 'openssl: ', extension_loaded('openssl') ? 'yes' : 'no', "\n";
//        echo 'http wrapper: ', in_array('http', $w) ? 'yes' : 'no', "\n";
//        echo 'https wrapper: ', in_array('https', $w) ? 'yes' : 'no', "\n";
//        echo 'wrappers: ', var_dump($w);
        // Se algum for false, habilitar as seguintes conf no php.ini
        //extension=php_openssl.dll
        //allow_url_include = On
        //allow_url_fopen = On

        $this->load->library('Kooaba');

        $file_path = $this->input->post('file_path');
        $file_name = $this->input->post('file_name');
        $title = $this->input->post('title');
        $metadata = NULL;

        $result = $this->kooaba->do_upload($file_path, $file_name, $title, $metadata);

        echo $result;
    }

    public function lookForVenues($venue, $city, $state) {

        $response = $this->foursquare_model->get($venue, $city, $state);

        if ($response->meta->code === 200) {

            $options = "<option value=''>.::Select::.</option>";

            foreach ($response->response->venues as $venue) {
                $options .= "<option value='" . $venue->id . "'>" . $venue->name . " | " . $venue->location->address . "</option>";
            }
        } else {
            $options = "<option>No venues found</option>";
        }

        echo $options;
    }

}

?>
