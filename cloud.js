var Shoes = Parse.Object.extend("Shoes");

Parse.Cloud.define("search", function(request, response) {
  
    var query = new Parse.Query(Shoes);
    
    query.matches("model", request.params.model).find({
        success: function(list){
            response.success(list);
        },
        error: function (obj, error){
            response.error("Erro ao buscar shoes: " + error.message);
        }
    });
});